﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Filters;
using KnowledgeAccountingSystem.BLL.Interfaces;
using KnowledgeAccountingSystem.BLL.Dto;


namespace KnowledgeAccountingSystem.API.ActionFilters
{
    public class ValidateResultExistsAttribute : IAsyncActionFilter
    {
        private readonly IResultService _resultService;

        public ValidateResultExistsAttribute(IResultService resultService)
        {
            _resultService = resultService;
        }

        public async Task OnActionExecutionAsync(ActionExecutingContext context, ActionExecutionDelegate next)
        {
            var resultId = context.ActionArguments.ContainsKey("result") ?
               ((ResultForUpdateDto)context.ActionArguments["result"]).Id :
               ((int)context.ActionArguments["id"]);

            var results = await _resultService.GetResultsWithoutUserAsync(false);
            var result = results.FirstOrDefault(r => r.Id == resultId);

            if (result == null)
                context.Result = new NotFoundResult();
            else
                await next();
        }
    }
}
