﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Filters;
using KnowledgeAccountingSystem.BLL.Interfaces;
using KnowledgeAccountingSystem.BLL.Dto;

namespace KnowledgeAccountingSystem.API.ActionFilters
{
    public class ValidateSubjectExistsAttribute : IAsyncActionFilter
    {
        private readonly ISubjectService _subjectService;

        public ValidateSubjectExistsAttribute(ISubjectService subjectService)
        {
            _subjectService = subjectService;
        }

        public async Task OnActionExecutionAsync(ActionExecutingContext context, ActionExecutionDelegate next)
        {
            int subjectId;

            if (context.RouteData.Values["controller"].ToString() == "Subjects")
                subjectId = context.ActionArguments.ContainsKey("subject") ?
                   ((SubjectForUpdateDto)context.ActionArguments["subject"]).Id :
                   ((int)context.ActionArguments["id"]);
            else
                subjectId = Convert.ToInt32(context.RouteData.Values["subjectId"]);

            var subjects = await _subjectService.GetSubjectsWithoutAreaAsync(false);
            var subject = subjects.FirstOrDefault(s => s.Id == subjectId);

            if (subject == null)
                context.Result = new NotFoundResult();
            else
                await next();
        }
    }
}
