﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Filters;
using KnowledgeAccountingSystem.BLL.Interfaces;
using KnowledgeAccountingSystem.BLL.Dto;

namespace KnowledgeAccountingSystem.API.ActionFilters
{
    public class ValidateAreaExistsAttribute : IAsyncActionFilter
    {
        private readonly IAreaService _areaService;

        public ValidateAreaExistsAttribute(IAreaService areaService)
        {
            _areaService = areaService;
        }

        public async Task OnActionExecutionAsync(ActionExecutingContext context, ActionExecutionDelegate next)
        {
            int areaId;

            if (context.RouteData.Values["controller"].ToString() == "Areas")
                areaId = context.ActionArguments.ContainsKey("area") ?
                   ((AreaForUpdateDto)context.ActionArguments["area"]).Id :
                   ((int)context.ActionArguments["id"]);
            else
                areaId = Convert.ToInt32(context.RouteData.Values["areaId"]);

            var area = await _areaService.GetAreaAsync(areaId, false);

            if (area == null)
                context.Result = new NotFoundResult();
            else
                await next();
        }
    }
}
