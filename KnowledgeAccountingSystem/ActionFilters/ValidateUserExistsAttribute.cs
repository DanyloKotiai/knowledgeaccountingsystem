﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Filters;
using Microsoft.AspNetCore.Identity;
using KnowledgeAccountingSystem.BLL.Interfaces;
using KnowledgeAccountingSystem.BLL.Dto;

namespace KnowledgeAccountingSystem.API.ActionFilters
{
    public class ValidateUserExistsAttribute : IAsyncActionFilter
    {
        private readonly IUserService _userService;

        public ValidateUserExistsAttribute(IUserService userService)
        {
            _userService = userService;
        }

        public async Task OnActionExecutionAsync(ActionExecutingContext context, ActionExecutionDelegate next)
        {
            Guid userId;

            if (context.RouteData.Values["controller"].ToString() == "Users")
                userId = context.ActionArguments.ContainsKey("user") ?
                   ((UserForEditingDto)context.ActionArguments["user"]).Id :
                   Guid.Parse(context.ActionArguments["id"].ToString());
            else
                userId = Guid.Parse(context.RouteData.Values["userId"].ToString());

            var user = await _userService.GetUserAsync(userId);

            if (user == null)
                context.Result = new NotFoundResult();
            else
                await next();
        }
    }
}
