﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Filters;
using KnowledgeAccountingSystem.BLL.Interfaces;
using KnowledgeAccountingSystem.BLL.Dto;

namespace KnowledgeAccountingSystem.API.ActionFilters
{
    public class ValidateAreaForResultExistsAttribute : IAsyncActionFilter
    {
        private readonly IAreaService _areaService;

        public ValidateAreaForResultExistsAttribute(IAreaService areaService )
        {
            _areaService = areaService;
        }

        public async Task OnActionExecutionAsync(ActionExecutingContext context, ActionExecutionDelegate next)
        {
            int areaId;

            var result = context.ActionArguments["result"];
            if (result is ResultForCreationDto)
                areaId = ((ResultForCreationDto)context.ActionArguments["result"]).AreaId;
            else
                areaId = ((ResultForUpdateDto)context.ActionArguments["result"]).AreaId;

            var area = await _areaService.GetAreaAsync(areaId, false);

            if (area == null)
                context.Result = new NotFoundResult();
            else
                await next();
        }
    }
}
