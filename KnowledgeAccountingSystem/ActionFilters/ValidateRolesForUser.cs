﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using KnowledgeAccountingSystem.BLL.Dto;
using KnowledgeAccountingSystem.BLL.Interfaces;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Filters;

namespace KnowledgeAccountingSystem.API.ActionFilters
{
    public class ValidateRolesForUser : IAsyncActionFilter
    {
        private readonly IUserService _userService;

        public ValidateRolesForUser(IUserService userService)
        {
            _userService = userService;
        }

        public async Task OnActionExecutionAsync(ActionExecutingContext context, ActionExecutionDelegate next)
        {
            var user = (UserForRegistrationDto)context.ActionArguments.SingleOrDefault(x => x.Value.ToString().Contains("Dto")).Value;

            if (user.Roles.Any())
            {
                var invalidRoles = await _userService.ValidateRolesForUserAsync(user.Roles);

                if (invalidRoles.Any())
                {
                    foreach (var role in invalidRoles)
                        context.ModelState.AddModelError(role.role, role.message);

                    context.Result = new BadRequestObjectResult(context.ModelState);
                }
                else
                    await next();
            }
            else
                await next();
        }
    }
}
