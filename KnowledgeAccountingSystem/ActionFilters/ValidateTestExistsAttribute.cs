﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Filters;
using KnowledgeAccountingSystem.BLL.Interfaces;
using KnowledgeAccountingSystem.BLL.Dto;

namespace KnowledgeAccountingSystem.API.ActionFilters
{
    public class ValidateTestExistsAttribute : IAsyncActionFilter
    {
        private readonly ITestService _testService;

        public ValidateTestExistsAttribute(ITestService testService)
        {
            _testService = testService;
        }

        public async Task OnActionExecutionAsync(ActionExecutingContext context, ActionExecutionDelegate next)
        {
            var testId = context.ActionArguments.ContainsKey("test") ?
               ((TestForUpdateDto)context.ActionArguments["test"]).Id :
               ((int)context.ActionArguments["id"]);

            var tests = await _testService.GetTestsWithoutSubjectAsync(false);
            var test = tests.FirstOrDefault(t => t.Id == testId);

            if (test == null)
                context.Result = new NotFoundResult();
            else
                await next();
        }
    }
}
