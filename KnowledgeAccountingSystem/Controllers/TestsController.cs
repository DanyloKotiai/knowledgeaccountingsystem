﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Authorization;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using KnowledgeAccountingSystem.API.ActionFilters;
using KnowledgeAccountingSystem.BLL.Interfaces;
using KnowledgeAccountingSystem.BLL.Dto;

namespace KnowledgeAccountingSystem.API.Controllers
{
    [Produces("application/json")]
    [Route("api/subjects/{subjectId:int}/tests")]
    [ApiController]
    [ServiceFilter(typeof(ValidateSubjectExistsAttribute))]
    [Authorize]
    public class TestsController : ControllerBase
    {
        private readonly ITestService _testService;

        public TestsController(ITestService testService)
        {
            _testService = testService;
        }


        /// <summary>
        /// This method returns all tests in the subject.
        /// </summary>
        /// <param name="subjectId">subject id</param>
        /// <returns>Tests</returns>
        [HttpGet]
        public async Task<IActionResult> GetTests(int subjectId)
        {
            var tests = await _testService.GetAllTestsAsync(subjectId, false);
            return Ok(tests);
        }

        /// <summary>
        /// Get test by id in the subject.
        /// </summary>
        /// <param name="subjectId">subject id</param>
        /// <param name="id">test id</param>
        /// <returns>Test</returns>
        [HttpGet("{id:int}", Name = "TestById")]
        public async Task<IActionResult> GetTest(int subjectId, int id)
        {
            var testDto = await _testService.GetTestAsync(subjectId, id, false);

            if (testDto == null) return NotFound();

            return Ok(testDto);
        }

        /// <summary>
        /// Create test by body data in the subject.
        /// </summary>
        /// <param name="subjectId">subject id</param>
        /// <param name="test">test</param>
        /// <returns>New test</returns>
        [HttpPost]
        [ServiceFilter(typeof(ValidationFilterAttribute))]
        public async Task<IActionResult> CreateSubject(int subjectId, [FromBody] TestForCreationDto test)
        {
            var testToReturn = await _testService.CreateTestForSubjectAsync(subjectId, test);

            return CreatedAtRoute("TestById", new { subjectId, id = testToReturn.Id }, testToReturn);
        }

        /// <summary>
        /// Update test by body data in the subject.
        /// </summary>
        /// <param name="subjectId">subject id</param>
        /// <param name="test">test</param>
        /// <returns>Status code 204</returns>
        [HttpPut]
        [ServiceFilter(typeof(ValidationFilterAttribute))]
        [ServiceFilter(typeof(ValidateTestExistsAttribute))]
        public async Task<IActionResult> UpdateSubject(int subjectId, TestForUpdateDto test)
        {
            await _testService.UpdateTestAsync(subjectId, test);

            return NoContent();
        }

        /// <summary>
        /// Delete test by id in the subject.
        /// </summary>
        /// <param name="id">test id</param>
        /// <returns>Status code 204</returns>
        [HttpDelete("{id:int}")]
        [ServiceFilter(typeof(ValidateTestExistsAttribute))]
        public async Task<IActionResult> DeleteSubject(int id)
        {
            await _testService.DeleteTestAsync(id);

            return NoContent();
        }
    }
}
