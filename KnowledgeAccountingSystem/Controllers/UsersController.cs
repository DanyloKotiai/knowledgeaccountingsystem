﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Authorization;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using KnowledgeAccountingSystem.API.ActionFilters;
using KnowledgeAccountingSystem.BLL.Interfaces;
using KnowledgeAccountingSystem.BLL.RequestFeatures;
using KnowledgeAccountingSystem.BLL.Dto;

namespace KnowledgeAccountingSystem.API.Controllers
{
    [Produces("application/json")]
    [Route("api/users")]
    [ApiController]
    public class UsersController : ControllerBase
    {
        private readonly IUserService _userService;

        public UsersController(IUserService userService)
        {
            _userService = userService;
        }


        /// <summary>
        /// This method returns all users.
        /// </summary>
        /// <returns>Users</returns>
        [HttpGet, Authorize]
        public async Task<IActionResult> GetUsers()
        {
            var usersDto = await _userService.GetAllUsersAsync();
            return Ok(usersDto);
        }

        /// <summary>
        /// Filtering users by parameters.
        /// </summary>
        /// <param name="parameters">parameters</param>
        /// <returns>Users</returns>
        [HttpGet("filtering"), Authorize(Roles = "Manager, Admin")]
        public async Task<IActionResult> GetUsersAndResults([FromQuery] UserParameters parameters)
        {
            var usersDto = await _userService.GetAllUsersWithResultsAsync(parameters);
            return Ok(usersDto);
        }

        /// <summary>
        /// Generate some statistic.
        /// </summary>
        /// <returns>Report</returns>
        [HttpGet("report"), Authorize(Roles = "Manager, Admin")]
        public async Task<IActionResult> GetReport()
        {
            var report = await _userService.GetReportAsync();

            return Ok(report);
        }

        /// <summary>
        /// Get user by email.
        /// </summary>
        /// <param name="email">email</param>
        /// <returns>User</returns>
        [HttpGet("byemail"), Authorize]
        public async Task<IActionResult> GetUserByEmail([FromQuery] string email)
        {
            var userDto = await _userService.GetUserByEmailAsync(email);

            if (userDto == null) return NotFound();

            return Ok(userDto);
        }

        /// <summary>
        /// Get all roles that users can have.
        /// </summary>
        /// <returns>Roles</returns>
        [HttpGet("roles"), Authorize(Roles = "Admin")]
        public async Task<IActionResult> GetRoles()
        {
            var roles = await _userService.GetRolesAsync();

            return Ok(roles);
        }

        /// <summary>
        /// Get user by id.
        /// </summary>
        /// <param name="id">user id</param>
        /// <returns>User</returns>
        [HttpGet("{id:guid}", Name = "UserById"), Authorize]
        public async Task<IActionResult> GetUser(Guid id)
        {
            var userDto = await _userService.GetUserAsync(id);

            if (userDto == null) return NotFound();

            return Ok(userDto);
        }

        /// <summary>
        /// Create user by body data.
        /// </summary>
        /// <param name="user">user</param>
        /// <returns>Status code 201</returns>
        [HttpPost, Authorize(Roles = "Admin")]
        [ServiceFilter(typeof(ValidationFilterAttribute))]
        [ServiceFilter(typeof(ValidateRolesForUser))]
        public async Task<IActionResult> CreateUser([FromBody] UserForRegistrationDto user)
        {
            var result = await _userService.CreateUserAsync(user);

            if (!result.Succeeded)
            {
                foreach (var err in result.Errors)
                    ModelState.TryAddModelError(err.Code, err.Description);

                return BadRequest(ModelState);
            }

            return StatusCode(201);
        }

        /// <summary>
        /// Update user by body data.
        /// </summary>
        /// <param name="user">user</param>
        /// <returns>Status code 204</returns>
        [HttpPut, Authorize(Roles = "Admin")]
        [ServiceFilter(typeof(ValidationFilterAttribute))]
        [ServiceFilter(typeof(ValidateUserExistsAttribute))]
        public async Task<IActionResult> EditUser([FromBody] UserForEditingDto user)
        {
            var result = await _userService.EditUserAsync(user);

            if (!result.Succeeded)
            {
                foreach (var err in result.Errors)
                    ModelState.TryAddModelError(err.Code, err.Description);

                return BadRequest(ModelState);
            }

            return NoContent();
        }

        /// <summary>
        /// Delete user by id.
        /// </summary>
        /// <param name="id">user id</param>
        /// <returns>Status code 204</returns>
        [HttpDelete("{id:guid}"), Authorize(Roles = "Admin")]
        [ServiceFilter(typeof(ValidateUserExistsAttribute))]
        public async Task<IActionResult> DeleteUser(Guid id)
        {
            var result = await _userService.DeleteUserAsync(id);

            if (!result)
                return BadRequest();

            return NoContent();
        }
    }
}
