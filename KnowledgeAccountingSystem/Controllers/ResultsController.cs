﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Authorization;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using KnowledgeAccountingSystem.API.ActionFilters;
using KnowledgeAccountingSystem.BLL.Interfaces;
using KnowledgeAccountingSystem.BLL.Dto;

namespace KnowledgeAccountingSystem.API.Controllers
{
    [Produces("application/json")]
    [Route("api/users/{userId:guid}/results")]
    [ApiController]
    [ServiceFilter(typeof(ValidateUserExistsAttribute))]
    [Authorize]
    public class ResultsController : ControllerBase
    {
        private readonly IResultService _resultService;

        public ResultsController(IResultService resultService)
        {
            _resultService = resultService;
        }


        /// <summary>
        /// This method returns all results for the user.
        /// </summary>
        /// <param name="userId">user id</param>
        /// <returns>Results</returns>
        [HttpGet]
        public async Task<IActionResult> GetResults(Guid userId)
        {
            var results = await _resultService.GetAllResultsAsync(userId, false);
            return Ok(results);
        }

        /// <summary>
        /// Get result by id for the user.
        /// </summary>
        /// <param name="userId">user id</param>
        /// <param name="id">result id</param>
        /// <returns>Result</returns>
        [HttpGet("{id:int}", Name = "ResultById")]
        public async Task<IActionResult> GetResult(Guid userId, int id)
        {
            var resultDto = await _resultService.GetResultAsync(userId, id, false);

            if (resultDto == null) return NotFound();

            return Ok(resultDto);
        }

        /// <summary>
        /// Create result by body data for the user.
        /// </summary>
        /// <param name="userId">user id</param>
        /// <param name="result">result</param>
        /// <returns>New result</returns>
        [HttpPost]
        [ServiceFilter(typeof(ValidationFilterAttribute))]
        [ServiceFilter(typeof(ValidateAreaForResultExistsAttribute))]
        public async Task<IActionResult> CreateResult(Guid userId, [FromBody] ResultForCreationDto result)
        {
            var resultToReturn = await _resultService.CreateResultForUserAsync(userId, result);

            return CreatedAtRoute("ResultById", new { userId, id = resultToReturn.Id }, resultToReturn);
        }

        /// <summary>
        /// Update result by body data for the user.
        /// </summary>
        /// <param name="userId">user id</param>
        /// <param name="result">result</param>
        /// <returns>Status code 204</returns>
        [HttpPut]
        [ServiceFilter(typeof(ValidationFilterAttribute))]
        [ServiceFilter(typeof(ValidateResultExistsAttribute))]
        [ServiceFilter(typeof(ValidateAreaForResultExistsAttribute))]
        public async Task<IActionResult> UpdateResult(Guid userId, ResultForUpdateDto result)
        {
            await _resultService.UpdateResultAsync(userId, result);

            return NoContent();
        }

        /// <summary>
        /// Delete result by id for the user.
        /// </summary>
        /// <param name="id">result id</param>
        /// <returns>Status code 204</returns>
        [HttpDelete("{id:int}")]
        [ServiceFilter(typeof(ValidateResultExistsAttribute))]
        public async Task<IActionResult> DeleteResult(int id)
        {
            await _resultService.DeleteResultAsync(id);

            return NoContent();
        }
    }
}
