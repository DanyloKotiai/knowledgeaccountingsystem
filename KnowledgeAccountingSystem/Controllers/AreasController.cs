﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Authorization;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using KnowledgeAccountingSystem.API.ActionFilters;
using KnowledgeAccountingSystem.BLL.Interfaces;
using KnowledgeAccountingSystem.BLL.Dto;

namespace KnowledgeAccountingSystem.API.Controllers
{
    [Produces("application/json")]
    [Route("api/areas")]
    [ApiController]
    [AllowAnonymous]
    public class AreasController : ControllerBase
    {
        private readonly IAreaService _areaService;

        public AreasController(IAreaService areaService)
        {
            _areaService = areaService;
        }


        /// <summary>
        /// This method returns all areas.
        /// </summary>
        /// <returns>Areas</returns>
        [HttpGet]
        public async Task<IActionResult> GetAreas()
        {
            var areasDto = await _areaService.GetAllAreasAsync(false);
            return Ok(areasDto);
        }

        /// <summary>
        /// Get area by id.
        /// </summary>
        /// <param name="id">area id</param>
        /// <returns>Area</returns>
        [HttpGet("{id:int}", Name = "AreaById")]
        public async Task<IActionResult> GetArea(int id)
        {
            var areaDto = await _areaService.GetAreaAsync(id, false);

            if (areaDto == null) return NotFound();

            return Ok(areaDto);
        }

        /// <summary>
        /// Create area by body data.
        /// </summary>
        /// <param name="area">area</param>
        /// <returns>New area</returns>
        [HttpPost]
        [ServiceFilter(typeof(ValidationFilterAttribute))]
        public async Task<IActionResult> CreateArea([FromBody] AreaForCreationDto area)
        {
            var areaToReturn = await _areaService.CreateAreaAsync(area);

            return CreatedAtRoute("AreaById", new { id = areaToReturn.Id }, areaToReturn);
        }

        /// <summary>
        /// Update area by body data.
        /// </summary>
        /// <param name="area">area</param>
        /// <returns>Status code 204</returns>
        [HttpPut]
        [ServiceFilter(typeof(ValidationFilterAttribute))]
        [ServiceFilter(typeof(ValidateAreaExistsAttribute))]
        public async Task<IActionResult> UpdateArea([FromBody] AreaForUpdateDto area)
        {
            await _areaService.UpdateAreaAsync(area);

            return NoContent();
        }

        /// <summary>
        /// Delete area by id.
        /// </summary>
        /// <param name="id">area id</param>
        /// <returns>Status code 204</returns>
        [HttpDelete("{id:int}")]
        [ServiceFilter(typeof(ValidateAreaExistsAttribute))]
        public async Task<IActionResult> DeleteArea(int id)
        {
            await _areaService.DeleteAreaAsync(id);

            return NoContent();
        }
    }
}
