﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Authorization;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using KnowledgeAccountingSystem.API.ActionFilters;
using KnowledgeAccountingSystem.BLL.Interfaces;
using KnowledgeAccountingSystem.BLL.Dto;

namespace KnowledgeAccountingSystem.API.Controllers
{
    [Produces("application/json")]
    [Route("api/areas/{areaId:int}/subjects")]
    [ApiController]
    [ServiceFilter(typeof(ValidateAreaExistsAttribute))]
    [Authorize]
    public class SubjectsController : ControllerBase
    {
        private readonly ISubjectService _subjectService;

        public SubjectsController(ISubjectService subjectService)
        {
            _subjectService = subjectService;
        }


        /// <summary>
        /// This method returns all subjects in the area.
        /// </summary>
        /// <param name="areaId">area id</param>
        /// <returns>Subjects</returns>
        [HttpGet]
        public async Task<IActionResult> GetSubjects(int areaId)
        {
            var subjects = await _subjectService.GetAllSubjectsAsync(areaId, false);
            return Ok(subjects);
        }

        /// <summary>
        /// Get subject by subject id in the area.
        /// </summary>
        /// <param name="areaId">area id</param>
        /// <param name="id">subject id</param>
        /// <returns>Subject</returns>
        [HttpGet("{id:int}", Name = "SubjectById")]
        public async Task<IActionResult> GetSubject(int areaId, int id)
        {
            var subjectDto = await _subjectService.GetSubjectAsync(areaId, id, false);

            if (subjectDto == null) return NotFound();

            return Ok(subjectDto);
        }

        /// <summary>
        /// Create subject by body data in the area.
        /// </summary>
        /// <param name="areaId">area id</param>
        /// <param name="subject">subject</param>
        /// <returns>New subject</returns>
        [HttpPost]
        [ServiceFilter(typeof(ValidationFilterAttribute))]
        public async Task<IActionResult> CreateSubject(int areaId, [FromBody] SubjectForCreationDto subject)
        {
            var subjectToReturn = await _subjectService.CreateSubjectForAreaAsync(areaId, subject);

            return CreatedAtRoute("SubjectById", new { areaId, id = subjectToReturn.Id}, subjectToReturn);
        }

        /// <summary>
        /// Update subject by body data in the area.
        /// </summary>
        /// <param name="areaId">area id</param>
        /// <param name="subject">subject</param>
        /// <returns>Status code 204</returns>
        [HttpPut]
        [ServiceFilter(typeof(ValidationFilterAttribute))]
        [ServiceFilter(typeof(ValidateSubjectExistsAttribute))]
        public async Task<IActionResult> UpdateSubject(int areaId, SubjectForUpdateDto subject)
        {
            await _subjectService.UpdateSubjectAsync(areaId, subject);

            return NoContent();
        }

        /// <summary>
        /// Delete subject by subject id in the area.
        /// </summary>
        /// <param name="id">subject id</param>
        /// <returns>Status code 204</returns>
        [HttpDelete("{id:int}")]
        [ServiceFilter(typeof(ValidateSubjectExistsAttribute))]
        public async Task<IActionResult> DeleteSubject(int id)
        {
            await _subjectService.DeleteSubjectAsync(id);

            return NoContent();
        }
    }
}
