﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Authorization;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using KnowledgeAccountingSystem.API.ActionFilters;
using KnowledgeAccountingSystem.BLL.Interfaces;
using KnowledgeAccountingSystem.BLL.Dto;
using Microsoft.AspNetCore.Identity;

namespace KnowledgeAccountingSystem.API.Controllers
{
    [Produces("application/json")]
    [Route("api/auth")]
    [ApiController]
    [AllowAnonymous]
    public class AuthenticationController : ControllerBase
    {
        private readonly IAuthenticationService _authenticationService;

        public AuthenticationController(IAuthenticationService authenticationService)
        {
            _authenticationService = authenticationService;
        }


        /// <summary>
        /// Register user by body data.
        /// </summary>
        /// <param name="user">user</param>
        /// <returns>Status code 201</returns>
        [HttpPost]
        [ServiceFilter(typeof(ValidationFilterAttribute))]
        public async Task<IActionResult> RegisterUser([FromBody] UserForRegistrationDto user)
        {
            var result = await _authenticationService.RegisterUserAsync(user);

            if (!result.Succeeded)
            {
                foreach (var err in result.Errors)
                    ModelState.TryAddModelError(err.Code, err.Description);

                return BadRequest(ModelState);
            }

            return StatusCode(201);
        }

        /// <summary>
        /// Log in user by body data.
        /// </summary>
        /// <param name="user">user</param>
        /// <returns>Token</returns>
        [HttpPost("login")]
        [ServiceFilter(typeof(ValidationFilterAttribute))]
        public async Task<IActionResult> SignIn([FromBody] UserForAuthenticationDto user)
        {
            if (!await _authenticationService.ValidateUserAsync(user))
                return Unauthorized();

            return Ok(new { Token = await _authenticationService.CreateTokenAsync() });
        }
    }
}
