﻿using System.Threading.Tasks;
using KnowledgeAccountingSystem.DAL.Interfaces;
using KnowledgeAccountingSystem.DAL.Repositories;

namespace KnowledgeAccountingSystem.DAL
{
    public class UnitOfWork : IUnitOfWork
    {
        private readonly KnowledgeAccountingDbContext _dbContext;
        private IAreaRepository _areaRepository;
        private ISubjectRepository _subjectRepository;
        private ITestRepository _testRepository;
        private IResultRepository _resultRepository;

        public UnitOfWork(KnowledgeAccountingDbContext dbContext)
        {
            _dbContext = dbContext;
        }


        public IAreaRepository AreaRepository
        {
            get
            {
                if (_areaRepository == null)
                    _areaRepository = new AreaRepository(_dbContext);

                return _areaRepository;
            }
        }

        public ISubjectRepository SubjectRepository
        {
            get
            {
                if (_subjectRepository == null)
                    _subjectRepository = new SubjectRepository(_dbContext);

                return _subjectRepository;
            }
        }

        public ITestRepository TestRepository
        {
            get
            {
                if (_testRepository == null)
                    _testRepository = new TestRepository(_dbContext);

                return _testRepository;
            }
        }

        public IResultRepository ResultRepository
        {
            get
            {
                if (_resultRepository == null)
                    _resultRepository = new ResultRepository(_dbContext);

                return _resultRepository;
            }
        }

        public async Task<int> SaveAsync()
            => await _dbContext.SaveChangesAsync();
    }
}
