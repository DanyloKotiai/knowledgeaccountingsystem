﻿using System.ComponentModel.DataAnnotations;

namespace KnowledgeAccountingSystem.DAL.Entities
{
    public class Test
    {
        public int Id { get; set; }

        [Required(ErrorMessage = "Text is a required field.")]
        public string Text { get; set; }

        [Required(ErrorMessage = "Score is a required field.")]
        public int Score { get; set; }

        [Required(ErrorMessage = "SubjectId is a required field.")]
        public int SubjectId { get; set; }

        public Subject Subject { get; set; }
    }
}
