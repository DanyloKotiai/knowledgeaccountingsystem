﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace KnowledgeAccountingSystem.DAL.Entities
{
    public class Area
    {
        public int Id { get; set; }

        [Required(ErrorMessage = "Name is a required field.")]
        [MaxLength(100, ErrorMessage = "Maximum length for the Name is 100 characters.")]
        public string Name { get; set; }

        public ICollection<Subject> Subjects { get; set; }
        public ICollection<Result> Results { get; set; }
    }
}
