﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace KnowledgeAccountingSystem.DAL.Entities
{
    public class Subject
    {
        public int Id { get; set; }

        [Required(ErrorMessage = "Name is a required field.")]
        [MaxLength(150, ErrorMessage = "Maximum length for the Name is 150 characters.")]
        public string Name { get; set; }

        [Required(ErrorMessage = "AreaId is a required field.")]
        public int AreaId { get; set; }

        public Area Area { get; set; }
        public ICollection<Test> Tests { get; set; }
    }
}
