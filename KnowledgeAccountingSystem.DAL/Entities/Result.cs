﻿using System;
using System.ComponentModel.DataAnnotations;

namespace KnowledgeAccountingSystem.DAL.Entities
{
    public class Result
    {
        public int Id { get; set; }

        [Required(ErrorMessage = "AreaId is a required field.")]
        public int AreaId { get; set; }

        [Required(ErrorMessage = "UserId is a required field.")]
        public string UserId { get; set; }

        [Required(ErrorMessage = "Date is a required field.")]
        public DateTime Date { get; set; }

        [Required(ErrorMessage = "Score is a required field.")]
        public int TotalScore { get; set; }

        public Area Area { get; set; }
        public User User { get; set; }
    }
}
