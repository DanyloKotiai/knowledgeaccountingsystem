﻿using System.Linq;
using KnowledgeAccountingSystem.DAL.Entities;
using KnowledgeAccountingSystem.DAL.Interfaces;
using Microsoft.EntityFrameworkCore;

namespace KnowledgeAccountingSystem.DAL.Repositories
{
    public class TestRepository : RepositoryBase<Test>, ITestRepository
    {
        public TestRepository(KnowledgeAccountingDbContext dbContext) : base(dbContext)
        {
        }

        public IQueryable<Test> FindAllWithDetails()
            => _dbContext.Tests.Include(t => t.Subject).ThenInclude(t => t.Area);
    }
}
