﻿using System.Linq;
using KnowledgeAccountingSystem.DAL.Entities;
using KnowledgeAccountingSystem.DAL.Interfaces;
using Microsoft.EntityFrameworkCore;

namespace KnowledgeAccountingSystem.DAL.Repositories
{
    public class ResultRepository : RepositoryBase<Result>, IResultRepository
    {
        public ResultRepository(KnowledgeAccountingDbContext dbContext) : base(dbContext)
        {
        }

        public IQueryable<Result> FindAllWithDetails()
            => _dbContext.Results.Include(t => t.User).Include(t => t.Area);
    }
}
