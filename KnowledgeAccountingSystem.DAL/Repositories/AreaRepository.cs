﻿using System.Threading.Tasks;
using KnowledgeAccountingSystem.DAL.Entities;
using KnowledgeAccountingSystem.DAL.Interfaces;
using Microsoft.EntityFrameworkCore;

namespace KnowledgeAccountingSystem.DAL.Repositories
{
    public class AreaRepository : RepositoryBase<Area>, IAreaRepository
    {
        public AreaRepository(KnowledgeAccountingDbContext dbContext) : base(dbContext)
        {
        }

        public async Task<Area> GetByIdAsync(int areaId, bool trackChanges)
            => trackChanges ? 
            await _dbContext.Areas.FirstOrDefaultAsync(t => t.Id == areaId) :
            await _dbContext.Areas.AsNoTracking().FirstOrDefaultAsync(t => t.Id == areaId);
    }
}
