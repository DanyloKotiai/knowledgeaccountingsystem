﻿using System.Linq;
using KnowledgeAccountingSystem.DAL.Entities;
using KnowledgeAccountingSystem.DAL.Interfaces;
using Microsoft.EntityFrameworkCore;

namespace KnowledgeAccountingSystem.DAL.Repositories
{
    public class SubjectRepository : RepositoryBase<Subject>, ISubjectRepository
    {
        public SubjectRepository(KnowledgeAccountingDbContext dbContext) : base(dbContext)
        {
        }

        public IQueryable<Subject> FindAllWithDetails()
            => _dbContext.Subjects.Include(t => t.Area);
    }
}
