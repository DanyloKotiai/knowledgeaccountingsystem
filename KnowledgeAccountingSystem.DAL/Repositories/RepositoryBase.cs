﻿using System;
using System.Linq;
using System.Linq.Expressions;
using Microsoft.EntityFrameworkCore;
using KnowledgeAccountingSystem.DAL.Interfaces;

namespace KnowledgeAccountingSystem.DAL.Repositories
{
    public abstract class RepositoryBase<TEntity> : IRepositoryBase<TEntity> where TEntity : class
    {
        protected KnowledgeAccountingDbContext _dbContext;

        protected RepositoryBase(KnowledgeAccountingDbContext dbContext)
        {
            _dbContext = dbContext;
        }


        public IQueryable<TEntity> FindAll(bool trackChanges)
            => trackChanges ?
            _dbContext.Set<TEntity>() :
            _dbContext.Set<TEntity>().AsNoTracking();

        public IQueryable<TEntity> FindByCondition(Expression<Func<TEntity, bool>> expression, bool trackChanges)
            => trackChanges ?
            _dbContext.Set<TEntity>().Where(expression) :
            _dbContext.Set<TEntity>().Where(expression).AsNoTracking();

        public void Create(TEntity entity)
            => _dbContext.Set<TEntity>().Add(entity);

        public void Update(TEntity entity)
            => _dbContext.Set<TEntity>().Update(entity);

        public void Delete(TEntity entity)
            => _dbContext.Set<TEntity>().Remove(entity);
    }
}
