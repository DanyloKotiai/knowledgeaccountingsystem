﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace KnowledgeAccountingSystem.DAL.Migrations
{
    public partial class DatabaseCreation : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "Areas",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Name = table.Column<string>(maxLength: 100, nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Areas", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "AspNetRoles",
                columns: table => new
                {
                    Id = table.Column<string>(nullable: false),
                    Name = table.Column<string>(maxLength: 256, nullable: true),
                    NormalizedName = table.Column<string>(maxLength: 256, nullable: true),
                    ConcurrencyStamp = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_AspNetRoles", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "AspNetUsers",
                columns: table => new
                {
                    Id = table.Column<string>(nullable: false),
                    UserName = table.Column<string>(maxLength: 256, nullable: true),
                    NormalizedUserName = table.Column<string>(maxLength: 256, nullable: true),
                    Email = table.Column<string>(maxLength: 256, nullable: true),
                    NormalizedEmail = table.Column<string>(maxLength: 256, nullable: true),
                    EmailConfirmed = table.Column<bool>(nullable: false),
                    PasswordHash = table.Column<string>(nullable: true),
                    SecurityStamp = table.Column<string>(nullable: true),
                    ConcurrencyStamp = table.Column<string>(nullable: true),
                    PhoneNumber = table.Column<string>(nullable: true),
                    PhoneNumberConfirmed = table.Column<bool>(nullable: false),
                    TwoFactorEnabled = table.Column<bool>(nullable: false),
                    LockoutEnd = table.Column<DateTimeOffset>(nullable: true),
                    LockoutEnabled = table.Column<bool>(nullable: false),
                    AccessFailedCount = table.Column<int>(nullable: false),
                    FirstName = table.Column<string>(maxLength: 50, nullable: false),
                    LastName = table.Column<string>(maxLength: 50, nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_AspNetUsers", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "Subjects",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Name = table.Column<string>(maxLength: 150, nullable: false),
                    AreaId = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Subjects", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Subjects_Areas_AreaId",
                        column: x => x.AreaId,
                        principalTable: "Areas",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "AspNetRoleClaims",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    RoleId = table.Column<string>(nullable: false),
                    ClaimType = table.Column<string>(nullable: true),
                    ClaimValue = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_AspNetRoleClaims", x => x.Id);
                    table.ForeignKey(
                        name: "FK_AspNetRoleClaims_AspNetRoles_RoleId",
                        column: x => x.RoleId,
                        principalTable: "AspNetRoles",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "AspNetUserClaims",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    UserId = table.Column<string>(nullable: false),
                    ClaimType = table.Column<string>(nullable: true),
                    ClaimValue = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_AspNetUserClaims", x => x.Id);
                    table.ForeignKey(
                        name: "FK_AspNetUserClaims_AspNetUsers_UserId",
                        column: x => x.UserId,
                        principalTable: "AspNetUsers",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "AspNetUserLogins",
                columns: table => new
                {
                    LoginProvider = table.Column<string>(nullable: false),
                    ProviderKey = table.Column<string>(nullable: false),
                    ProviderDisplayName = table.Column<string>(nullable: true),
                    UserId = table.Column<string>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_AspNetUserLogins", x => new { x.LoginProvider, x.ProviderKey });
                    table.ForeignKey(
                        name: "FK_AspNetUserLogins_AspNetUsers_UserId",
                        column: x => x.UserId,
                        principalTable: "AspNetUsers",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "AspNetUserRoles",
                columns: table => new
                {
                    UserId = table.Column<string>(nullable: false),
                    RoleId = table.Column<string>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_AspNetUserRoles", x => new { x.UserId, x.RoleId });
                    table.ForeignKey(
                        name: "FK_AspNetUserRoles_AspNetRoles_RoleId",
                        column: x => x.RoleId,
                        principalTable: "AspNetRoles",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_AspNetUserRoles_AspNetUsers_UserId",
                        column: x => x.UserId,
                        principalTable: "AspNetUsers",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "AspNetUserTokens",
                columns: table => new
                {
                    UserId = table.Column<string>(nullable: false),
                    LoginProvider = table.Column<string>(nullable: false),
                    Name = table.Column<string>(nullable: false),
                    Value = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_AspNetUserTokens", x => new { x.UserId, x.LoginProvider, x.Name });
                    table.ForeignKey(
                        name: "FK_AspNetUserTokens_AspNetUsers_UserId",
                        column: x => x.UserId,
                        principalTable: "AspNetUsers",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "Results",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    AreaId = table.Column<int>(nullable: false),
                    UserId = table.Column<string>(nullable: false),
                    Date = table.Column<DateTime>(nullable: false),
                    TotalScore = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Results", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Results_Areas_AreaId",
                        column: x => x.AreaId,
                        principalTable: "Areas",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_Results_AspNetUsers_UserId",
                        column: x => x.UserId,
                        principalTable: "AspNetUsers",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "Tests",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Text = table.Column<string>(nullable: false),
                    Score = table.Column<int>(nullable: false),
                    SubjectId = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Tests", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Tests_Subjects_SubjectId",
                        column: x => x.SubjectId,
                        principalTable: "Subjects",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.InsertData(
                table: "Areas",
                columns: new[] { "Id", "Name" },
                values: new object[,]
                {
                    { 1, "Computer Science" },
                    { 2, "Software Engineering" },
                    { 3, "Programming" },
                    { 4, "Experience" }
                });

            migrationBuilder.InsertData(
                table: "AspNetRoles",
                columns: new[] { "Id", "ConcurrencyStamp", "Name", "NormalizedName" },
                values: new object[,]
                {
                    { "a563c85f-a5be-4a8b-a377-c29bdf3afc8e", "ac843d30-ac69-4515-bd59-2071da78dd27", "Manager", "MANAGER" },
                    { "62d59300-1382-41e1-b9b7-16086e80761e", "5333bacc-009b-4714-8702-3110ecfd3c9e", "Admin", "ADMIN" }
                });

            migrationBuilder.InsertData(
                table: "Subjects",
                columns: new[] { "Id", "AreaId", "Name" },
                values: new object[,]
                {
                    { 1, 1, "Data structures" },
                    { 2, 1, "Algorithms" },
                    { 3, 1, "Systems programming" },
                    { 4, 2, "Source code version control" },
                    { 5, 2, "Build automation" },
                    { 6, 2, "Automated testing" },
                    { 7, 3, "Code readability" },
                    { 8, 3, "Frameworks" },
                    { 9, 3, "Database" },
                    { 10, 4, "Platforms with professional experience" },
                    { 11, 4, "Years of professional experience" },
                    { 12, 4, "Languages with professional experience" }
                });

            migrationBuilder.InsertData(
                table: "Tests",
                columns: new[] { "Id", "Score", "SubjectId", "Text" },
                values: new object[,]
                {
                    { 1, 0, 1, "Doesn’t know the difference between Array and LinkedList" },
                    { 27, 2, 7, "No long functions, comments explaining unusual code" },
                    { 28, 3, 7, "Code flows naturally – no deep nesting of conditionals or methods" },
                    { 29, 0, 8, "Has not used any framework outside of the core platform" },
                    { 30, 1, 8, "Has heard about but not used the popular frameworks available for the platform." },
                    { 31, 2, 8, "Has used more than one framework in a professional capacity" },
                    { 32, 3, 8, "Author of framework" },
                    { 33, 0, 9, "Thinks that Excel is a database" },
                    { 34, 1, 9, "Knows basic database concepts, normalization and can write simple selects" },
                    { 35, 2, 9, "Able to design good and normalized database. Proficient in use of ORM tools" },
                    { 26, 1, 7, "Good names for files, variables classes, methods etc." },
                    { 36, 3, 9, "Can do basic database administration, performance optimization, index optimization, write advanced select queries" },
                    { 38, 1, 10, "2-3" },
                    { 39, 2, 10, "4-5" },
                    { 40, 3, 10, "6+" },
                    { 41, 0, 11, "1" },
                    { 42, 1, 11, "2-5" },
                    { 43, 2, 11, "6-9" },
                    { 44, 3, 11, "10+" },
                    { 45, 0, 12, "Imperative or Object Oriented" },
                    { 46, 1, 12, "Imperative, Object-Oriented and declarative (SQL)" },
                    { 37, 0, 10, "1" },
                    { 25, 0, 7, "Doesn't hear about code readability" },
                    { 24, 3, 6, "Understands how to setup automated functional, load/performance and UI tests" },
                    { 23, 2, 6, "Has written code in TDD manner" },
                    { 2, 1, 1, "Able to explain and use Arrays, LinkedLists, Dictionaries" },
                    { 3, 2, 1, "Able to explain how hashtables can be implemented and can handle collisions" },
                    { 4, 3, 1, "Knowledge of advanced data structures like B-trees, fibonacci heaps, AVL/Red Black trees" },
                    { 5, 0, 2, "Unable to find the average of numbers in an array" },
                    { 6, 1, 2, "Basic sorting, searching algorithms" },
                    { 7, 2, 2, "Tree, Graph algorithms" },
                    { 8, 3, 2, "Good knowledge of graph algorithms, good knowledge of numerical computation algorithms" },
                    { 9, 0, 3, "Doesn’t know what a compiler or interpreter is" },
                    { 10, 1, 3, "Basic understanding of compilers and interpreters." },
                    { 11, 2, 3, "Able to read assembly code. Understands how networks work, understanding of network protocols and socket level programming." },
                    { 12, 3, 3, "Understands the entire programming stack, hardware, binary code, compilation, interpretation, JIT compilation, garbage collection, heap, stack" },
                    { 13, 0, 4, "Folder backups by date" },
                    { 14, 1, 4, "VSS and beginning CVS/SVN user" },
                    { 15, 2, 4, "Proficient in using CVS and SVN features" },
                    { 16, 3, 4, "Knowledge of distributed VCS systems" },
                    { 17, 0, 5, "Only knows how to build from IDE" },
                    { 18, 1, 5, "Knows how to build the system from the command line" },
                    { 19, 2, 5, "Can setup a script to build the basic system" },
                    { 20, 3, 5, "Can setup a script to build the system and generate release notes and tag the code in source control" },
                    { 21, 0, 6, "Thinks that all testing is the job of the tester" },
                    { 22, 1, 6, "Has written automated unit tests" },
                    { 47, 2, 12, "Functional" },
                    { 48, 3, 12, "Concurrent and Logig" }
                });

            migrationBuilder.CreateIndex(
                name: "IX_AspNetRoleClaims_RoleId",
                table: "AspNetRoleClaims",
                column: "RoleId");

            migrationBuilder.CreateIndex(
                name: "RoleNameIndex",
                table: "AspNetRoles",
                column: "NormalizedName",
                unique: true,
                filter: "[NormalizedName] IS NOT NULL");

            migrationBuilder.CreateIndex(
                name: "IX_AspNetUserClaims_UserId",
                table: "AspNetUserClaims",
                column: "UserId");

            migrationBuilder.CreateIndex(
                name: "IX_AspNetUserLogins_UserId",
                table: "AspNetUserLogins",
                column: "UserId");

            migrationBuilder.CreateIndex(
                name: "IX_AspNetUserRoles_RoleId",
                table: "AspNetUserRoles",
                column: "RoleId");

            migrationBuilder.CreateIndex(
                name: "EmailIndex",
                table: "AspNetUsers",
                column: "NormalizedEmail");

            migrationBuilder.CreateIndex(
                name: "UserNameIndex",
                table: "AspNetUsers",
                column: "NormalizedUserName",
                unique: true,
                filter: "[NormalizedUserName] IS NOT NULL");

            migrationBuilder.CreateIndex(
                name: "IX_Results_AreaId",
                table: "Results",
                column: "AreaId");

            migrationBuilder.CreateIndex(
                name: "IX_Results_UserId",
                table: "Results",
                column: "UserId");

            migrationBuilder.CreateIndex(
                name: "IX_Subjects_AreaId",
                table: "Subjects",
                column: "AreaId");

            migrationBuilder.CreateIndex(
                name: "IX_Tests_SubjectId",
                table: "Tests",
                column: "SubjectId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "AspNetRoleClaims");

            migrationBuilder.DropTable(
                name: "AspNetUserClaims");

            migrationBuilder.DropTable(
                name: "AspNetUserLogins");

            migrationBuilder.DropTable(
                name: "AspNetUserRoles");

            migrationBuilder.DropTable(
                name: "AspNetUserTokens");

            migrationBuilder.DropTable(
                name: "Results");

            migrationBuilder.DropTable(
                name: "Tests");

            migrationBuilder.DropTable(
                name: "AspNetRoles");

            migrationBuilder.DropTable(
                name: "AspNetUsers");

            migrationBuilder.DropTable(
                name: "Subjects");

            migrationBuilder.DropTable(
                name: "Areas");
        }
    }
}
