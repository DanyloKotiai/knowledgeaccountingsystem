﻿using KnowledgeAccountingSystem.DAL.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace KnowledgeAccountingSystem.DAL.Configuration
{
    public class AreaConfiguration : IEntityTypeConfiguration<Area>
    {
        public void Configure(EntityTypeBuilder<Area> builder)
        {
            builder.HasData
            (
                new Area
                {
                    Id = 1,
                    Name = "Computer Science",
                },
                new Area
                {
                    Id = 2,
                    Name = "Software Engineering",
                },
                new Area
                {
                    Id = 3,
                    Name = "Programming",
                },
                new Area
                {
                    Id = 4,
                    Name = "Experience",
                }
            );
        }
    }
}
