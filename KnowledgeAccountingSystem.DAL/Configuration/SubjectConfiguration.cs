﻿using KnowledgeAccountingSystem.DAL.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace KnowledgeAccountingSystem.DAL.Configuration
{
    public class SubjectConfiguration : IEntityTypeConfiguration<Subject>
    {
        public void Configure(EntityTypeBuilder<Subject> builder)
        {
            builder.HasData
            (
                new Subject
                {
                    Id = 1,
                    Name = "Data structures",
                    AreaId = 1,
                },
                new Subject
                {
                    Id = 2,
                    Name = "Algorithms",
                    AreaId = 1,
                },
                new Subject
                {
                    Id = 3,
                    Name = "Systems programming",
                    AreaId = 1,
                },
                new Subject
                {
                    Id = 4,
                    Name = "Source code version control",
                    AreaId = 2,
                },
                new Subject
                {
                    Id = 5,
                    Name = "Build automation",
                    AreaId = 2,
                },
                new Subject
                {
                    Id = 6,
                    Name = "Automated testing",
                    AreaId = 2,
                },
                new Subject
                {
                    Id = 7,
                    Name = "Code readability",
                    AreaId = 3,
                },
                new Subject
                {
                    Id = 8,
                    Name = "Frameworks",
                    AreaId = 3,
                },
                new Subject
                {
                    Id = 9,
                    Name = "Database",
                    AreaId = 3,
                },
                new Subject
                {
                    Id = 10,
                    Name = "Platforms with professional experience",
                    AreaId = 4,
                },
                new Subject
                {
                    Id = 11,
                    Name = "Years of professional experience",
                    AreaId = 4,
                },
                new Subject
                {
                    Id = 12,
                    Name = "Languages with professional experience",
                    AreaId = 4,
                }
            );
        }
    }
}
