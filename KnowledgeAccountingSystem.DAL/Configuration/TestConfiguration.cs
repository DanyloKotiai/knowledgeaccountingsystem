﻿using KnowledgeAccountingSystem.DAL.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace KnowledgeAccountingSystem.DAL.Configuration
{
    public class TestConfiguration : IEntityTypeConfiguration<Test>
    {
        public void Configure(EntityTypeBuilder<Test> builder)
        {
            builder.HasData
            (
                new Test
                {
                    Id = 1,
                    Text = "Doesn’t know the difference between Array and LinkedList",
                    Score = 0,
                    SubjectId = 1
                },
                new Test
                {
                    Id = 2,
                    Text = "Able to explain and use Arrays, LinkedLists, Dictionaries",
                    Score = 1,
                    SubjectId = 1
                },
                new Test
                {
                    Id = 3,
                    Text = "Able to explain how hashtables can be implemented and can handle collisions",
                    Score = 2,
                    SubjectId = 1
                },
                new Test
                {
                    Id = 4,
                    Text = "Knowledge of advanced data structures like B-trees, fibonacci heaps, AVL/Red Black trees",
                    Score = 3,
                    SubjectId = 1
                },
                new Test
                {
                    Id = 5,
                    Text = "Unable to find the average of numbers in an array",
                    Score = 0,
                    SubjectId = 2
                },
                new Test
                {
                    Id = 6,
                    Text = "Basic sorting, searching algorithms",
                    Score = 1,
                    SubjectId = 2
                },
                new Test
                {
                    Id = 7,
                    Text = "Tree, Graph algorithms",
                    Score = 2,
                    SubjectId = 2
                },
                new Test
                {
                    Id = 8,
                    Text = "Good knowledge of graph algorithms, good knowledge of numerical computation algorithms",
                    Score = 3,
                    SubjectId = 2
                },
                new Test
                {
                    Id = 9,
                    Text = "Doesn’t know what a compiler or interpreter is",
                    Score = 0,
                    SubjectId = 3
                },
                new Test
                {
                    Id = 10,
                    Text = "Basic understanding of compilers and interpreters.",
                    Score = 1,
                    SubjectId = 3
                },
                new Test
                {
                    Id = 11,
                    Text = "Able to read assembly code. Understands how networks work, understanding of network protocols and socket level programming.",
                    Score = 2,
                    SubjectId = 3
                },
                new Test
                {
                    Id = 12,
                    Text = "Understands the entire programming stack, hardware, binary code, compilation, interpretation, JIT compilation, garbage collection, heap, stack",
                    Score = 3,
                    SubjectId = 3
                },
                new Test
                {
                    Id = 13,
                    Text = "Folder backups by date",
                    Score = 0,
                    SubjectId = 4
                },
                new Test
                {
                    Id = 14,
                    Text = "VSS and beginning CVS/SVN user",
                    Score = 1,
                    SubjectId = 4
                },
                new Test
                {
                    Id = 15,
                    Text = "Proficient in using CVS and SVN features",
                    Score = 2,
                    SubjectId = 4
                },
                new Test
                {
                    Id = 16,
                    Text = "Knowledge of distributed VCS systems",
                    Score = 3,
                    SubjectId = 4
                },
                new Test
                {
                    Id = 17,
                    Text = "Only knows how to build from IDE",
                    Score = 0,
                    SubjectId = 5
                },
                new Test
                {
                    Id = 18,
                    Text = "Knows how to build the system from the command line",
                    Score = 1,
                    SubjectId = 5
                },
                new Test
                {
                    Id = 19,
                    Text = "Can setup a script to build the basic system",
                    Score = 2,
                    SubjectId = 5
                },
                new Test
                {
                    Id = 20,
                    Text = "Can setup a script to build the system and generate release notes and tag the code in source control",
                    Score = 3,
                    SubjectId = 5
                },
                new Test
                {
                    Id = 21,
                    Text = "Thinks that all testing is the job of the tester",
                    Score = 0,
                    SubjectId = 6
                },
                new Test
                {
                    Id = 22,
                    Text = "Has written automated unit tests",
                    Score = 1,
                    SubjectId = 6
                },
                new Test
                {
                    Id = 23,
                    Text = "Has written code in TDD manner",
                    Score = 2,
                    SubjectId = 6
                },
                new Test
                {
                    Id = 24,
                    Text = "Understands how to setup automated functional, load/performance and UI tests",
                    Score = 3,
                    SubjectId = 6
                },
                new Test
                {
                    Id = 25,
                    Text = "Doesn't hear about code readability",
                    Score = 0,
                    SubjectId = 7
                },
                new Test
                {
                    Id = 26,
                    Text = "Good names for files, variables classes, methods etc.",
                    Score = 1,
                    SubjectId = 7
                },
                new Test
                {
                    Id = 27,
                    Text = "No long functions, comments explaining unusual code",
                    Score = 2,
                    SubjectId = 7
                },
                new Test
                {
                    Id = 28,
                    Text = "Code flows naturally – no deep nesting of conditionals or methods",
                    Score = 3,
                    SubjectId = 7
                },
                new Test
                {
                    Id = 29,
                    Text = "Has not used any framework outside of the core platform",
                    Score = 0,
                    SubjectId = 8
                },
                new Test
                {
                    Id = 30,
                    Text = "Has heard about but not used the popular frameworks available for the platform.",
                    Score = 1,
                    SubjectId = 8
                },
                new Test
                {
                    Id = 31,
                    Text = "Has used more than one framework in a professional capacity",
                    Score = 2,
                    SubjectId = 8
                },
                new Test
                {
                    Id = 32,
                    Text = "Author of framework",
                    Score = 3,
                    SubjectId = 8
                },
                new Test
                {
                    Id = 33,
                    Text = "Thinks that Excel is a database",
                    Score = 0,
                    SubjectId = 9
                },
                new Test
                {
                    Id = 34,
                    Text = "Knows basic database concepts, normalization and can write simple selects",
                    Score = 1,
                    SubjectId = 9
                },
                new Test
                {
                    Id = 35,
                    Text = "Able to design good and normalized database. Proficient in use of ORM tools",
                    Score = 2,
                    SubjectId = 9
                },
                new Test
                {
                    Id = 36,
                    Text = "Can do basic database administration, performance optimization, index optimization, write advanced select queries",
                    Score = 3,
                    SubjectId = 9
                },
                new Test
                {
                    Id = 37,
                    Text = "1",
                    Score = 0,
                    SubjectId = 10
                },
                new Test
                {
                    Id = 38,
                    Text = "2-3",
                    Score = 1,
                    SubjectId = 10
                },
                new Test
                {
                    Id = 39,
                    Text = "4-5",
                    Score = 2,
                    SubjectId = 10
                },
                new Test
                {
                    Id = 40,
                    Text = "6+",
                    Score = 3,
                    SubjectId = 10
                },
                new Test
                {
                    Id = 41,
                    Text = "1",
                    Score = 0,
                    SubjectId = 11
                },
                new Test
                {
                    Id = 42,
                    Text = "2-5",
                    Score = 1,
                    SubjectId = 11
                },
                new Test
                {
                    Id = 43,
                    Text = "6-9",
                    Score = 2,
                    SubjectId = 11
                },
                new Test
                {
                    Id = 44,
                    Text = "10+",
                    Score = 3,
                    SubjectId = 11
                },
                new Test
                {
                    Id = 45,
                    Text = "Imperative or Object Oriented",
                    Score = 0,
                    SubjectId = 12
                },
                new Test
                {
                    Id = 46,
                    Text = "Imperative, Object-Oriented and declarative (SQL)",
                    Score = 1,
                    SubjectId = 12
                },
                new Test
                {
                    Id = 47,
                    Text = "Functional",
                    Score = 2,
                    SubjectId = 12
                },
                new Test
                {
                    Id = 48,
                    Text = "Concurrent and Logig",
                    Score = 3,
                    SubjectId = 12
                }
            );
        }
    }
}
