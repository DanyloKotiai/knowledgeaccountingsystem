﻿using System.Linq;
using KnowledgeAccountingSystem.DAL.Entities;

namespace KnowledgeAccountingSystem.DAL.Interfaces
{
    public interface IResultRepository : IRepositoryBase<Result>
    {
        IQueryable<Result> FindAllWithDetails();
    }
}
