﻿using System.Linq;
using KnowledgeAccountingSystem.DAL.Entities;

namespace KnowledgeAccountingSystem.DAL.Interfaces
{
    public interface ITestRepository : IRepositoryBase<Test>
    {
        IQueryable<Test> FindAllWithDetails();
    }
}
