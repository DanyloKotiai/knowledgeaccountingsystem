﻿using System.Threading.Tasks;
using KnowledgeAccountingSystem.DAL.Entities;

namespace KnowledgeAccountingSystem.DAL.Interfaces
{
    public interface IAreaRepository : IRepositoryBase<Area>
    {
        Task<Area> GetByIdAsync(int areaId, bool trackChanges);
    }
}
