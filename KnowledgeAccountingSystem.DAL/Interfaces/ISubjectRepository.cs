﻿using System.Linq;
using KnowledgeAccountingSystem.DAL.Entities;

namespace KnowledgeAccountingSystem.DAL.Interfaces
{
    public interface ISubjectRepository : IRepositoryBase<Subject>
    {
        IQueryable<Subject> FindAllWithDetails();
    }
}
