﻿using System.Threading.Tasks;

namespace KnowledgeAccountingSystem.DAL.Interfaces
{
    public interface IUnitOfWork
    {
        IAreaRepository AreaRepository { get; }
        ISubjectRepository SubjectRepository { get; }
        ITestRepository TestRepository { get; }
        IResultRepository ResultRepository { get; }

        Task<int> SaveAsync();
    }
}
