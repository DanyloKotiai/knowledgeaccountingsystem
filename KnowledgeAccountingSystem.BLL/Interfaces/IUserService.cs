﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using KnowledgeAccountingSystem.BLL.Dto;
using KnowledgeAccountingSystem.BLL.RequestFeatures;
using Microsoft.AspNetCore.Identity;

namespace KnowledgeAccountingSystem.BLL.Interfaces
{
    public interface IUserService
    {
        Task<IEnumerable<UserDto>> GetAllUsersAsync();
        Task<IEnumerable<UserResultDto>> GetAllUsersWithResultsAsync(UserParameters parameters);
        Task<ReportDto> GetReportAsync();
        Task<IEnumerable<string>> GetRolesAsync();
        Task<UserDto> GetUserByEmailAsync(string email);
        Task<UserDto> GetUserAsync(Guid id);
        Task<IdentityResult> CreateUserAsync(UserForRegistrationDto user);
        Task<IdentityResult> EditUserAsync(UserForEditingDto user);
        Task<bool> DeleteUserAsync(Guid id);
        Task<IEnumerable<(string role, string message)>> ValidateRolesForUserAsync(IEnumerable<string> roles);
    }
}
