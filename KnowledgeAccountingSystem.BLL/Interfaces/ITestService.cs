﻿using System.Collections.Generic;
using System.Threading.Tasks;
using KnowledgeAccountingSystem.BLL.Dto;

namespace KnowledgeAccountingSystem.BLL.Interfaces
{
    public interface ITestService
    {
        Task<IEnumerable<TestDto>> GetAllTestsAsync(int subjectId, bool trackChanges);
        Task<IEnumerable<TestDto>> GetTestsWithoutSubjectAsync(bool trackChanges);
        Task<TestDto> GetTestAsync(int subjectId, int testId, bool trackChanges);
        Task<TestDto> CreateTestForSubjectAsync(int subjectId, TestForCreationDto test);
        Task UpdateTestAsync(int subjectId, TestForUpdateDto test);
        Task DeleteTestAsync(int testId);
    }
}
