﻿using System.Collections.Generic;
using System.Threading.Tasks;
using KnowledgeAccountingSystem.BLL.Dto;

namespace KnowledgeAccountingSystem.BLL.Interfaces
{
    public interface ISubjectService
    {
        Task<IEnumerable<SubjectDto>> GetAllSubjectsAsync(int areaId, bool trackChanges);
        Task<IEnumerable<SubjectDto>> GetSubjectsWithoutAreaAsync(bool trackChanges);
        Task<SubjectDto> GetSubjectAsync(int areaId, int subjectId, bool trackChanges);
        Task<SubjectDto> CreateSubjectForAreaAsync(int areaId, SubjectForCreationDto subject);
        Task UpdateSubjectAsync(int areaId, SubjectForUpdateDto subject);
        Task DeleteSubjectAsync(int subjectId);
    }
}
