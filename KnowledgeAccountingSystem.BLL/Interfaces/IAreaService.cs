﻿using System.Collections.Generic;
using System.Threading.Tasks;
using KnowledgeAccountingSystem.BLL.Dto;

namespace KnowledgeAccountingSystem.BLL.Interfaces
{
    public interface IAreaService
    {
        Task<IEnumerable<AreaDto>> GetAllAreasAsync(bool trackChanges);
        Task<AreaDto> GetAreaAsync(int areaId, bool trackChanges);
        Task<AreaDto> CreateAreaAsync(AreaForCreationDto area);
        Task UpdateAreaAsync(AreaForUpdateDto area);
        Task DeleteAreaAsync(int areaId);
    }
}
