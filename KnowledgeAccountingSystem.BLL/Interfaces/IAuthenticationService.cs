﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Identity;
using KnowledgeAccountingSystem.BLL.Dto;

namespace KnowledgeAccountingSystem.BLL.Interfaces
{
    public interface IAuthenticationService
    {
        Task<IdentityResult> RegisterUserAsync(UserForRegistrationDto userForRegistration);
        Task<bool> ValidateUserAsync(UserForAuthenticationDto userForAuth);
        Task<string> CreateTokenAsync();
    }
}
