﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using KnowledgeAccountingSystem.BLL.Dto;

namespace KnowledgeAccountingSystem.BLL.Interfaces
{
    public interface IResultService
    {
        Task<IEnumerable<ResultDto>> GetAllResultsAsync(Guid userId, bool trackChanges);
        Task<ResultDto> GetResultAsync(Guid userId, int resultId, bool trackChanges);
        Task<IEnumerable<ResultDto>> GetResultsWithoutUserAsync(bool trackChanges);
        Task<ResultDto> CreateResultForUserAsync(Guid userId, ResultForCreationDto result);
        Task UpdateResultAsync(Guid userId, ResultForUpdateDto result);
        Task DeleteResultAsync(int resultId);
    }
}
