﻿using System;
using System.Collections.Generic;

namespace KnowledgeAccountingSystem.BLL.RequestFeatures
{
    public class UserParameters
    {
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Email { get; set; }
        public ICollection<string> AreaNames { get; set; } = new List<string>();
        public int MinResult { get; set; } = 0;
        public int MaxResult { get; set; } = int.MaxValue;
        public DateTime Date { get; set; } = new DateTime(2021, 1, 1);
    }
}
