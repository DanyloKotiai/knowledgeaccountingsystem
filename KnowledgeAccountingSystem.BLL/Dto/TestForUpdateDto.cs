﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace KnowledgeAccountingSystem.BLL.Dto
{
    public class TestForUpdateDto
    {
        [Range(1, int.MaxValue, ErrorMessage = "Id is a required field.")]
        public int Id { get; set; }

        [Required(ErrorMessage = "Text is a required field.")]
        public string Text { get; set; }

        [Range(1, int.MaxValue, ErrorMessage = "Score is a required field.")]
        public int Score { get; set; }
    }
}
