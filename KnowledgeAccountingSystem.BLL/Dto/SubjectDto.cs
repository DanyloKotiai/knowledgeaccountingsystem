﻿using System;
using System.Collections.Generic;

namespace KnowledgeAccountingSystem.BLL.Dto
{
    public class SubjectDto
    {
        public int Id { get; set; }
        public string Name { get; set; }
    }
}
