﻿using System;
using System.Collections.Generic;

namespace KnowledgeAccountingSystem.BLL.Dto
{
    public class ReportDto
    {
        public int UsersCount { get; set; }
        public int ResultsCount { get; set; }
    }
}
