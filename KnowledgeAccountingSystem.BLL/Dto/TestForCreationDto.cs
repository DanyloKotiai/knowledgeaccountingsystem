﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace KnowledgeAccountingSystem.BLL.Dto
{
    public class TestForCreationDto
    {
        [Required(ErrorMessage = "Test text is a required field.")]
        public string Text { get; set; }

        [Range(1, int.MaxValue, ErrorMessage = "Score is a required field.")]
        public int Score { get; set; }
    }
}
