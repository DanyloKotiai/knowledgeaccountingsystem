﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace KnowledgeAccountingSystem.BLL.Dto
{
    public class AreaForCreationDto
    {
        [Required(ErrorMessage = "Area name is a required field.")]
        [MaxLength(60, ErrorMessage = "Maximum length for the Name is 60 characters.")]
        public string Name { get; set; }
    }
}
