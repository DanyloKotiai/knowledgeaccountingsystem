﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace KnowledgeAccountingSystem.BLL.Dto
{
    public class ResultForUpdateDto
    {
        [Range(1, int.MaxValue, ErrorMessage = "Id is a required field.")]
        public int Id { get; set; }

        [Range(1, int.MaxValue, ErrorMessage = "AreaId is a required field.")]
        public int AreaId { get; set; }

        [DataType(DataType.Date)]
        [Range(typeof(DateTime), "1/1/2010", "1/1/2050", ErrorMessage = "Date must be greater than 1/1/2010 and lower than 1/1/2050.")]
        public DateTime Date { get; set; }

        [Range(1, int.MaxValue, ErrorMessage = "TotalScore is a required field.")]
        public int TotalScore { get; set; }
    }
}
