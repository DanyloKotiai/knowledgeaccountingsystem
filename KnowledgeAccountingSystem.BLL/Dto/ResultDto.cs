﻿using System;
using System.Collections.Generic;

namespace KnowledgeAccountingSystem.BLL.Dto
{
    public class ResultDto
    {
        public int Id { get; set; }
        public int AreaId { get; set; }
        public string AreaName { get; set; }
        public DateTime Date { get; set; }
        public int TotalScore { get; set; }
    }
}
