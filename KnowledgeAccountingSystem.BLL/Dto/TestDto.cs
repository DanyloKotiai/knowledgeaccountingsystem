﻿using System;
using System.Collections.Generic;

namespace KnowledgeAccountingSystem.BLL.Dto
{
    public class TestDto
    {
        public int Id { get; set; }
        public string Text { get; set; }
        public int Score { get; set; }
    }
}
