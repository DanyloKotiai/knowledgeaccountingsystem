﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace KnowledgeAccountingSystem.BLL.Dto
{
    public class AreaForUpdateDto
    {
        [Required(ErrorMessage = "Id is a required field.")]
        public int Id { get; set; }

        [Required(ErrorMessage = "Area name is a required field.")]
        [MaxLength(60, ErrorMessage = "Maximum length for the Name is 60 characters.")]
        public string Name { get; set; }
    }
}
