﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace KnowledgeAccountingSystem.BLL.Dto
{
    public class SubjectForUpdateDto
    {
        [Range(1, int.MaxValue, ErrorMessage = "Id is a required field.")]
        public int Id { get; set; }

        [Required(ErrorMessage = "Subject name is a required field.")]
        [MaxLength(150, ErrorMessage = "Maximum length for the Name is 150 characters.")]
        public string Name { get; set; }
    }
}
