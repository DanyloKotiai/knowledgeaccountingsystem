﻿using System;
using System.Runtime.Serialization;

namespace KnowledgeAccountingSystem.BLL.Exceptions
{
    [Serializable]
    public class KnowledgeAccountingSystemException : Exception
    {
        public KnowledgeAccountingSystemException(string message) : base(message)
        {
        }

        protected KnowledgeAccountingSystemException(SerializationInfo info, StreamingContext context) : base(info, context)
        {
        }
    }
}
