﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using AutoMapper;
using KnowledgeAccountingSystem.BLL.Exceptions;
using KnowledgeAccountingSystem.BLL.Interfaces;
using KnowledgeAccountingSystem.BLL.Dto;
using KnowledgeAccountingSystem.DAL.Interfaces;
using KnowledgeAccountingSystem.DAL.Entities;

namespace KnowledgeAccountingSystem.BLL.Services
{
    public class TestService : ITestService
    {
        private readonly IUnitOfWork _unitOfWork;
        private readonly IMapper _mapper;

        public TestService(IUnitOfWork unitOfWork, IMapper mapper)
        {
            _unitOfWork = unitOfWork;
            _mapper = mapper;
        }


        public async Task<IEnumerable<TestDto>> GetAllTestsAsync(int subjectId, bool trackChanges)
        {
            var tests = await _unitOfWork.TestRepository.FindByCondition(t => t.SubjectId == subjectId, trackChanges).ToListAsync();
            return _mapper.Map<IEnumerable<TestDto>>(tests);
        }

        public async Task<IEnumerable<TestDto>> GetTestsWithoutSubjectAsync(bool trackChanges)
        {
            var tests = await _unitOfWork.TestRepository.FindAll(trackChanges).ToListAsync();
            return _mapper.Map<IEnumerable<TestDto>>(tests);
        }

        public async Task<TestDto> GetTestAsync(int subjectId, int testId, bool trackChanges)
        {
            var test = await _unitOfWork.TestRepository.FindByCondition(t => t.SubjectId == subjectId, trackChanges)
                     .SingleOrDefaultAsync(t => t.Id == testId);
            return test != null ? _mapper.Map<TestDto>(test) : null;
        }

        public async Task<TestDto> CreateTestForSubjectAsync(int subjectId, TestForCreationDto test)
        {
            if (test == null) throw new KnowledgeAccountingSystemException("Test is null.");
            if (string.IsNullOrEmpty(test.Text)) throw new KnowledgeAccountingSystemException("Test text is null or empty.");
            if (test.Score < 1) throw new KnowledgeAccountingSystemException("Test score is invalid.");

            var testEntity = _mapper.Map<Test>(test);
            testEntity.SubjectId = subjectId;
            _unitOfWork.TestRepository.Create(testEntity);

            await _unitOfWork.SaveAsync();

            var testToReturn = _mapper.Map<TestDto>(testEntity);
            return testToReturn;
        }

        public async Task UpdateTestAsync(int subjectId, TestForUpdateDto test)
        {
            if (test == null) throw new KnowledgeAccountingSystemException("Test is null.");
            if (test.Id < 1) throw new KnowledgeAccountingSystemException("Test id is invalid.");
            if (string.IsNullOrEmpty(test.Text)) throw new KnowledgeAccountingSystemException("Test text is null or empty.");
            if (test.Score < 1) throw new KnowledgeAccountingSystemException("Test score is invalid.");

            var testEntity = _mapper.Map<Test>(test);
            testEntity.SubjectId = subjectId;
            _unitOfWork.TestRepository.Update(testEntity); 

            await _unitOfWork.SaveAsync();
        }

        public async Task DeleteTestAsync(int testId)
        {
            var testEntity = await _unitOfWork.TestRepository.FindByCondition(s => s.Id == testId, false).SingleOrDefaultAsync()
                 ?? throw new KnowledgeAccountingSystemException("This test is not exist.");
            _unitOfWork.TestRepository.Delete(testEntity);

            await _unitOfWork.SaveAsync();
        }
    }
}
