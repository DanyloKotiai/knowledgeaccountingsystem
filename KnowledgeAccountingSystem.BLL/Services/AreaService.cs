﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using AutoMapper;
using KnowledgeAccountingSystem.BLL.Exceptions;
using KnowledgeAccountingSystem.BLL.Interfaces;
using KnowledgeAccountingSystem.BLL.Dto;
using KnowledgeAccountingSystem.DAL.Interfaces;
using KnowledgeAccountingSystem.DAL.Entities;

namespace KnowledgeAccountingSystem.BLL.Services
{
    public class AreaService : IAreaService
    {
        private readonly IUnitOfWork _unitOfWork;
        private readonly IMapper _mapper;

        public AreaService(IUnitOfWork unitOfWork, IMapper mapper)
        {
            _unitOfWork = unitOfWork;
            _mapper = mapper;
        }


        public async Task<IEnumerable<AreaDto>> GetAllAreasAsync(bool trackChanges)
        {
            var areas = await _unitOfWork.AreaRepository.FindAll(trackChanges).OrderBy(a => a.Name).ToListAsync();
            return _mapper.Map<IEnumerable<AreaDto>>(areas);
        }

        public async Task<AreaDto> GetAreaAsync(int areaId, bool trackChanges)
        {
            var area = await _unitOfWork.AreaRepository.GetByIdAsync(areaId, trackChanges);
            return area != null ? _mapper.Map<AreaDto>(area) : null;
        }

        public async Task<AreaDto> CreateAreaAsync(AreaForCreationDto area)
        {
            if (area == null) throw new KnowledgeAccountingSystemException("Area is null.");
            if (string.IsNullOrEmpty(area.Name)) throw new KnowledgeAccountingSystemException("Area name is null or empty.");
            if (area.Name.Length > 60) throw new KnowledgeAccountingSystemException("Area name has length more than must.");

            var areaEntity = _mapper.Map<Area>(area);
            _unitOfWork.AreaRepository.Create(areaEntity);

            await _unitOfWork.SaveAsync();

            var areaToReturn = _mapper.Map<AreaDto>(areaEntity);
            return areaToReturn;
        }

        public async Task UpdateAreaAsync(AreaForUpdateDto area)
        {
            if (area == null) throw new KnowledgeAccountingSystemException("Area is null.");
            if (area.Id < 1) throw new KnowledgeAccountingSystemException("Area id is invalid.");
            if (string.IsNullOrEmpty(area.Name)) throw new KnowledgeAccountingSystemException("Area name is null or empty.");
            if (area.Name.Length > 60) throw new KnowledgeAccountingSystemException("Area name has length more than must.");

            var areaEntity = _mapper.Map<Area>(area);
            _unitOfWork.AreaRepository.Update(areaEntity);

            await _unitOfWork.SaveAsync();
        }

        public async Task DeleteAreaAsync(int areaId)
        {
            var areaEntity = await _unitOfWork.AreaRepository.GetByIdAsync(areaId, true) ?? throw new KnowledgeAccountingSystemException("This area is not exist.");
            _unitOfWork.AreaRepository.Delete(areaEntity);

            await _unitOfWork.SaveAsync();
        }
    }
}
