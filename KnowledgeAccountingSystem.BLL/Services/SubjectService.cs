﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using AutoMapper;
using KnowledgeAccountingSystem.BLL.Exceptions;
using KnowledgeAccountingSystem.BLL.Interfaces;
using KnowledgeAccountingSystem.BLL.Dto;
using KnowledgeAccountingSystem.DAL.Interfaces;
using KnowledgeAccountingSystem.DAL.Entities;

namespace KnowledgeAccountingSystem.BLL.Services
{
    public class SubjectService : ISubjectService
    {
        private readonly IUnitOfWork _unitOfWork;
        private readonly IMapper _mapper;

        public SubjectService(IUnitOfWork unitOfWork, IMapper mapper)
        {
            _unitOfWork = unitOfWork;
            _mapper = mapper;
        }


        public async Task<IEnumerable<SubjectDto>> GetAllSubjectsAsync(int areaId, bool trackChanges)
        {
            var subjects = await _unitOfWork.SubjectRepository.FindByCondition(s => s.AreaId == areaId, trackChanges).ToListAsync();
            return _mapper.Map<IEnumerable<SubjectDto>>(subjects);
        }

        public async Task<IEnumerable<SubjectDto>> GetSubjectsWithoutAreaAsync(bool trackChanges)
        {
            var subjects = await _unitOfWork.SubjectRepository.FindAll(trackChanges).ToListAsync();
            return _mapper.Map<IEnumerable<SubjectDto>>(subjects);
        }

        public async Task<SubjectDto> GetSubjectAsync(int areaId, int subjectId, bool trackChanges)
        {
            var subject = await _unitOfWork.SubjectRepository.FindByCondition(s => s.AreaId == areaId, trackChanges)
                    .SingleOrDefaultAsync(s => s.Id == subjectId);
            return subject != null ? _mapper.Map<SubjectDto>(subject) : null;
        }

        public async Task<SubjectDto> CreateSubjectForAreaAsync(int areaId, SubjectForCreationDto subject)
        {
            if (subject == null) throw new KnowledgeAccountingSystemException("Subject is null.");
            if (string.IsNullOrEmpty(subject.Name)) throw new KnowledgeAccountingSystemException("Subject name is null or empty.");
            if (subject.Name.Length > 150) throw new KnowledgeAccountingSystemException("Subject name has length more than must.");

            var subjectEntity = _mapper.Map<Subject>(subject);
            subjectEntity.AreaId = areaId;
            _unitOfWork.SubjectRepository.Create(subjectEntity);

            await _unitOfWork.SaveAsync();

            var subjectToReturn = _mapper.Map<SubjectDto>(subjectEntity);
            return subjectToReturn;
        }

        public async Task UpdateSubjectAsync(int areaId, SubjectForUpdateDto subject)
        {
            if (subject == null) throw new KnowledgeAccountingSystemException("Subject is null.");
            if (subject.Id < 1) throw new KnowledgeAccountingSystemException("Subject id is invalid.");
            if (string.IsNullOrEmpty(subject.Name)) throw new KnowledgeAccountingSystemException("Subject name is null or empty.");
            if (subject.Name.Length > 150) throw new KnowledgeAccountingSystemException("Subject name has length more than must.");

            var subjectEntity = _mapper.Map<Subject>(subject);
            subjectEntity.AreaId = areaId;
            _unitOfWork.SubjectRepository.Update(subjectEntity);

            await _unitOfWork.SaveAsync();
        }

        public async Task DeleteSubjectAsync(int subjectId)
        {
            var subjectEntity = await _unitOfWork.SubjectRepository.FindByCondition(s => s.Id == subjectId, false).SingleOrDefaultAsync()
                 ?? throw new KnowledgeAccountingSystemException("This subject is not exist.");
            _unitOfWork.SubjectRepository.Delete(subjectEntity);

            await _unitOfWork.SaveAsync();
        }
    }
}
