﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using AutoMapper;
using KnowledgeAccountingSystem.BLL.Exceptions;
using KnowledgeAccountingSystem.BLL.Interfaces;
using KnowledgeAccountingSystem.BLL.Dto;
using KnowledgeAccountingSystem.DAL.Interfaces;
using KnowledgeAccountingSystem.DAL.Entities;

namespace KnowledgeAccountingSystem.BLL.Services
{
    public class ResultService : IResultService
    {
        private readonly IUnitOfWork _unitOfWork;
        private readonly IMapper _mapper;

        public ResultService(IUnitOfWork unitOfWork, IMapper mapper)
        {
            _unitOfWork = unitOfWork;
            _mapper = mapper;
        }


        public async Task<IEnumerable<ResultDto>> GetAllResultsAsync(Guid userId, bool trackChanges)
        {
            var results = await _unitOfWork.ResultRepository.FindByCondition(r => r.UserId == userId.ToString(), trackChanges).Include(r => r.Area)
                .ToListAsync();
            return _mapper.Map<IEnumerable<ResultDto>>(results);
        }

        public async Task<IEnumerable<ResultDto>> GetResultsWithoutUserAsync(bool trackChanges)
        {
            var results = await _unitOfWork.ResultRepository.FindAll(trackChanges).ToListAsync();
            return _mapper.Map<IEnumerable<ResultDto>>(results);
        }

        public async Task<ResultDto> GetResultAsync(Guid userId, int resultId, bool trackChanges)
        {
            var result = await _unitOfWork.ResultRepository.FindByCondition(r => r.UserId == userId.ToString(), trackChanges).Include(r => r.Area)
                .SingleOrDefaultAsync(r => r.Id == resultId);
            return result != null ? _mapper.Map<ResultDto>(result) : null;
        }

        public async Task<ResultDto> CreateResultForUserAsync(Guid userId, ResultForCreationDto result)
        {
            if (result == null) throw new KnowledgeAccountingSystemException("Result is null.");
            if (result.AreaId < 1) throw new KnowledgeAccountingSystemException("AreaId is invalid.");
            if (result.Date < new DateTime(2010, 1, 1) || result.Date > new DateTime(2050, 1, 1)) throw new KnowledgeAccountingSystemException("Date is lower than 1/1/2010 or greater than 1/1/2050.");
            if (result.TotalScore < 0) throw new KnowledgeAccountingSystemException("TotalScore is invalid.");

            var resultEntity = _mapper.Map<Result>(result);
            resultEntity.UserId = userId.ToString();
            _unitOfWork.ResultRepository.Create(resultEntity);

            await _unitOfWork.SaveAsync();

            var resultToReturn = _mapper.Map<ResultDto>(await _unitOfWork.ResultRepository.FindAllWithDetails().FirstOrDefaultAsync(r => r.Id == resultEntity.Id));
            return resultToReturn;
        }

        public async Task UpdateResultAsync(Guid userId, ResultForUpdateDto result)
        {
            if (result == null) throw new KnowledgeAccountingSystemException("Result is null.");
            if (result.Id < 1) throw new KnowledgeAccountingSystemException("Result id is invalid.");
            if (result.AreaId < 1) throw new KnowledgeAccountingSystemException("AreaId is invalid.");
            if (result.Date < new DateTime(2010, 1, 1) || result.Date > new DateTime(2050, 1, 1)) throw new KnowledgeAccountingSystemException("Date is lower than 1/1/2010 or greater than 1/1/2050.");
            if (result.TotalScore < 0) throw new KnowledgeAccountingSystemException("TotalScore is invalid.");

            var resultEntity = _mapper.Map<Result>(result);
            resultEntity.UserId = userId.ToString();
            _unitOfWork.ResultRepository.Update(resultEntity);

            await _unitOfWork.SaveAsync();
        }

        public async Task DeleteResultAsync(int resultId)
        {
            var resultEntity = await _unitOfWork.ResultRepository.FindByCondition(r => r.Id == resultId, false).SingleOrDefaultAsync()
                ?? throw new KnowledgeAccountingSystemException("This result is not exist.");
            _unitOfWork.ResultRepository.Delete(resultEntity);

            await _unitOfWork.SaveAsync();
        }
    }
}
