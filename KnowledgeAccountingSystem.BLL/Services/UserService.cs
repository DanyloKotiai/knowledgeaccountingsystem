﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using Microsoft.AspNetCore.Identity;
using AutoMapper;
using KnowledgeAccountingSystem.BLL.Exceptions;
using KnowledgeAccountingSystem.BLL.Interfaces;
using KnowledgeAccountingSystem.BLL.RequestFeatures;
using KnowledgeAccountingSystem.BLL.Dto;
using KnowledgeAccountingSystem.DAL.Interfaces;
using KnowledgeAccountingSystem.DAL.Entities;
using System.Diagnostics;

namespace KnowledgeAccountingSystem.BLL.Services
{
    public class UserService : IUserService
    {
        private readonly UserManager<User> _userManager;
        private readonly RoleManager<IdentityRole> _roleManager;
        private readonly IAuthenticationService _authenticationService;
        private readonly IMapper _mapper;

        public UserService(UserManager<User> userManager, RoleManager<IdentityRole> roleManager, IAuthenticationService authenticationService, IMapper mapper)
        {
            _userManager = userManager;
            _roleManager = roleManager;
            _authenticationService = authenticationService;
            _mapper = mapper;
        }


        public async Task<IEnumerable<UserDto>> GetAllUsersAsync()
        {
            var users = await _userManager.Users.ToListAsync();

            var listOfRoles = new List<IList<string>>();
            foreach (var user in users)
                listOfRoles.Add(await _userManager.GetRolesAsync(user));

            var usersDto = _mapper.Map<IEnumerable<UserDto>>(users).ToList();
            for (int i = 0; i < usersDto.Count; i++)
                usersDto[i].Roles = listOfRoles[i];

            return usersDto;
        }

        public async Task<IEnumerable<UserResultDto>> GetAllUsersWithResultsAsync(UserParameters parameters)
        {
            var tempUsers = await _userManager.Users
                .Where(u => string.IsNullOrEmpty(parameters.FirstName) || u.FirstName == parameters.FirstName)
                .Where(u => string.IsNullOrEmpty(parameters.LastName) || u.LastName == parameters.LastName)
                .Where(u => string.IsNullOrEmpty(parameters.Email) || u.Email == parameters.Email)
                .Include(u => u.Results)
                .ThenInclude(r => r.Area)
                .ToListAsync();

            var users = tempUsers.Select(u =>
            {
                u.Results = u.Results
                    .Where(t => t.TotalScore >= parameters.MinResult &&
                                t.TotalScore <= parameters.MaxResult &&
                                t.Date >= parameters.Date)
                    .Where(t => !parameters.AreaNames.Any() ||
                                parameters.AreaNames.Contains(t.Area.Name)).ToList();
                return u;
            }).ToList();

            return _mapper.Map<IEnumerable<User>, IEnumerable<UserResultDto>>(users);
        }

        public async Task<ReportDto> GetReportAsync()
        {
            var tempUsers = await _userManager.Users
                .Include(u => u.Results)
                .ToListAsync();

            var users = tempUsers.Select(u =>
            {
                u.Results = u.Results.Where(r => r.Date >= DateTime.Now.AddDays(-7)).ToList();
                return u;
            });

            var usersCount = users.Count(u => u.Results.Any());
            var resultsCount = users.Sum(u => u.Results.Count);

            return new ReportDto { UsersCount = usersCount, ResultsCount = resultsCount };
        }

        public async Task<IEnumerable<string>> GetRolesAsync()
        {
            var roles =  await _roleManager.Roles.Select(r => r.Name).ToListAsync();
            return roles;
        }

        public async Task<UserDto> GetUserByEmailAsync(string email)
        {
            var user = await _userManager.Users.FirstOrDefaultAsync(u => u.Email == email);

            return _mapper.Map<UserDto>(user);
        }

        public async Task<UserDto> GetUserAsync(Guid id)
        {
            var user = await _userManager.FindByIdAsync(id.ToString());
            return user != null ? _mapper.Map<UserDto>(user) : null;
        }

        public async Task<IdentityResult> CreateUserAsync(UserForRegistrationDto user)
        {
            var result = await _authenticationService.RegisterUserAsync(user);
            return result;
        }

        public async Task<IdentityResult> EditUserAsync(UserForEditingDto user)
        {
            var userEntity = await _userManager.FindByIdAsync(user.Id.ToString())
                ?? throw new KnowledgeAccountingSystemException("User is not exist.");

            _mapper.Map(user, userEntity);

            var userRoles = await _userManager.GetRolesAsync(userEntity);
            var addedRoles = user.Roles.Except(userRoles);
            var removedRoles = userRoles.Except(user.Roles);

            await _userManager.AddToRolesAsync(userEntity, addedRoles);
            await _userManager.RemoveFromRolesAsync(userEntity, removedRoles);

            var result = await _userManager.UpdateAsync(userEntity);
            return result;
        }

        public async Task<bool> DeleteUserAsync(Guid id)
        {
            var userEntity = await _userManager.FindByIdAsync(id.ToString())
                ?? throw new KnowledgeAccountingSystemException("User is not exist.");

            var result = await _userManager.DeleteAsync(userEntity);
            return result.Succeeded;
        }

        public async Task<IEnumerable<(string role, string message)>> ValidateRolesForUserAsync(IEnumerable<string> roles)
        {
            var invalidRoles = new List<(string, string)>();
            foreach (var role in roles)
            {
                var isValid = await _roleManager.RoleExistsAsync(role);

                if (!isValid)
                    invalidRoles.Add((role, "This role is not valid."));
            }

            return invalidRoles;
        }
    }
}
