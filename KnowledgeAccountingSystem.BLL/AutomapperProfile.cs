﻿using AutoMapper;
using KnowledgeAccountingSystem.BLL.Dto;
using KnowledgeAccountingSystem.DAL.Entities;

namespace KnowledgeAccountingSystem.BLL
{
    public class AutomapperProfile : Profile
    {
        public AutomapperProfile()
        {
            CreateMap<Area, AreaDto>().ReverseMap();
            CreateMap<AreaForCreationDto, Area>();
            CreateMap<AreaForUpdateDto, Area>();

            CreateMap<Subject, SubjectDto>().ReverseMap();
            CreateMap<SubjectForCreationDto, Subject>();
            CreateMap<SubjectForUpdateDto, Subject>();

            CreateMap<Test, TestDto>().ReverseMap();
            CreateMap<TestForCreationDto, Test>();
            CreateMap<TestForUpdateDto, Test>();


            CreateMap<User, UserDto>();
            CreateMap<UserForRegistrationDto, User>()
                .ForMember(d => d.UserName, c => c.MapFrom(u => u.Email));
            CreateMap<UserForEditingDto, User>()
                .ForMember(d => d.UserName, c => c.MapFrom(u => u.Email))
                .ForMember(d => d.Id, c => c.MapFrom(u => u.Id.ToString()));

            CreateMap<Result, ResultDto>()
                .ForMember(d => d.AreaName, c => c.MapFrom(r => r.Area.Name));
            CreateMap<ResultForCreationDto, Result>();
            CreateMap<ResultForUpdateDto, Result>();

            CreateMap<User, UserResultDto>();
        }
    }
}
