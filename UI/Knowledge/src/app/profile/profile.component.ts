import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { ResultService } from '../services/result.service';
import { UserService } from '../services/user.service';

@Component({
  selector: 'app-profile',
  templateUrl: './profile.component.html',
  styleUrls: ['./profile.component.css']
})

export class ProfileComponent implements OnInit {
  FirstName: string = '';
  LastName: string = '';
  ListOfResults: any = [];

  constructor(private userService: UserService, private resultService: ResultService) { }

  async ngOnInit(): Promise<void> {
    this.ListOfResults = await this.resultService.showResultsForUser();
    console.log(this.ListOfResults);
    const claims = this.userService.getClaimsForUser();
    this.FirstName = claims[0];
    this.LastName = claims[1];
  }
}
