import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { NgForm } from '@angular/forms';
import { JwtHelperService } from '@auth0/angular-jwt';
import { UserService } from './user.service';

@Injectable()
export class ResultService {
  baseUrl: string = 'https:localhost:44351/api';

  constructor(private http: HttpClient, private jwtHelper: JwtHelperService, private userService: UserService) { }

  async addResult(form: NgForm, areaId: number) {
    const dateNow = new Date();
    let totalScore: number = 0;

    for (const key in form.value) {
      totalScore = totalScore + Number(form.value[key]);
    }

    const value = {
      'date': dateNow,
      'areaId': Number(areaId),
      'totalScore': totalScore
    };

    const userId = await this.userService.getUserIdByEmail();

    await this.http.post(`${this.baseUrl}/users/${userId}/results`, value).toPromise()
      .then(data => {
        console.log(data);
      })
      .catch(err => {
        console.log(err.error);
      })

  }

  async showResultsForUser() {
    const userId = await this.userService.getUserIdByEmail();
    let listOfResults: Array<any> = [];

    await this.http.get<any>(`${this.baseUrl}/users/${userId}/results`).toPromise()
      .then(data => {
        listOfResults = data;
      })
      .catch(err => {
        console.log(err.error);
      });

    listOfResults = listOfResults.map(item => {
      let date = new Date(item.date);

      item.date = date.toLocaleString("en", {
        year: 'numeric',
        month: 'long',
        day: 'numeric',
        hour: 'numeric',
        minute: 'numeric',
      });
      return item;
    }).reverse();

    return listOfResults;
  }

  async showResultsForUsersByFiltering(form: any) {
    return this.userService.getResultsForUsersByFiltering(form);
  }
}
