import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';

@Injectable()
export class AreaService {
  baseUrl: string = 'https:localhost:44351/api';

  constructor(private http: HttpClient) { }

  getAreas(): Observable<any> {
    return this.http.get<any>(`${this.baseUrl}/areas`);
  }

  getSubjectByAreaId(areaId: number): Observable<any> {
    return this.http.get<any>(`${this.baseUrl}/areas/${areaId}/subjects`);
  }

  getTestBySubjectId(subjectId: number): Observable<any> {
    return this.http.get<any>(`${this.baseUrl}/subjects/${subjectId}/tests`);
  }
}
