import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { JwtHelperService } from '@auth0/angular-jwt';
import { Observable } from 'rxjs';

@Injectable()
export class UserService {

  baseUrl: string = 'https:localhost:44351/api';

  constructor(private http: HttpClient, private jwtHelper: JwtHelperService) { }

  getUsers(): Observable<any> {
    return this.http.get<any>(`${this.baseUrl}/users`);
  }

  async getUserIdByEmail(): Promise<any> {
    let token: any = localStorage.getItem('jwt');

    const decodedToken = this.jwtHelper.decodeToken(token);
    const user = decodedToken['http://schemas.xmlsoap.org/ws/2005/05/identity/claims/name'];

    let data: any;
    await this.http.get<any>(`${this.baseUrl}/users/byemail?email=${user[2]}`).toPromise()
      .then(value => {
        data = value;
      })
      .catch(err => {
        console.log(err.error);
      });

    return data.id;
  }

  getClaimsForUser() {
    let token: any = localStorage.getItem('jwt');
    const decodedToken = this.jwtHelper.decodeToken(token);

    return decodedToken['http://schemas.xmlsoap.org/ws/2005/05/identity/claims/name'];
  }

  async getRoles() {
    return this.http.get<any>(`${this.baseUrl}/users/roles`).toPromise();
  }

  async getResultsForUsersByFiltering(value: any): Promise<any> {
    let listOfAreas: Array<any> = value.areaNames;
    let areaNamesString;

    if (listOfAreas[0] === undefined) {
      areaNamesString = '';
    } else {
      areaNamesString = listOfAreas.map((item, index, array) =>
        `areaNames[${index}]=${item}`).join('&');
    }

    let data: any;
    const filteringString = `firstname=${value.firstname}&lastname=${value.lastname}&email=${value.email}&minResult=${value.minResult}&maxResult=${value.maxResult}&date=${value.date}&${areaNamesString}`;
    await this.http.get<any>(`${this.baseUrl}/users/filtering?${filteringString}`).toPromise()
      .then(val => {
        data = val;
      })
      .catch(err => {
        console.log(err.error);
      });


    return data;
  }

  async getReportData() {
    let data: any;

    await this.http.get<any>(`${this.baseUrl}/users/report`).toPromise()
      .then(val => {
        data = val;
      })
      .catch(err => {
        console.log(err.error);
      });

    return data;
  }

  async createUser(formValue: any) {
    return this.http.post(`${this.baseUrl}/users`, formValue).toPromise();
  }

  async editUser(formValue: any) {
    return this.http.put(`${this.baseUrl}/users`, formValue).toPromise();
  }

  async deleteUser(id: any) {
    await this.http.delete(`${this.baseUrl}/users/${id}`).toPromise().catch(err => {
      console.log(err.error);
    });
  }
}
