import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { NgForm } from '@angular/forms';
import { Router } from '@angular/router';

@Injectable()
export class AuthService {
  isValid: boolean = false;

  constructor(private router: Router, private http: HttpClient) { }

  async login(form: NgForm): Promise<boolean> {
    const credentials = {
      'email': form.value.email,
      'password': form.value.password
    }

    await this.http.post('https://localhost:44351/api/auth/login', credentials).toPromise()
      .then(value => {
        const token = (<any>value).token;
        localStorage.setItem('jwt', token);
        this.isValid = true;
      })
      .catch(err => {
        console.log(err.error);
        this.isValid = false;
      });

    return this.isValid;
  }

  async signUp(form: NgForm): Promise<boolean> {
    const credentials = {
      'firstname': form.value.firstname,
      'lastname': form.value.lastname,
      'email': form.value.email,
      'password': form.value.password
    }

    await this.http.post('https://localhost:44351/api/auth', credentials).toPromise()
      .then(value => {
        this.isValid = true;
      })
      .catch(err => {
        console.log(err.error);
        this.isValid = false;
      });


    return this.isValid;
  }
}
