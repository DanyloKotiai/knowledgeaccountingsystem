import { Injectable } from '@angular/core';
import { CanActivate, Router } from '@angular/router';
import { JwtHelperService } from '@auth0/angular-jwt';

@Injectable()
export class AuthGuardManagerService implements CanActivate {

  constructor(private router: Router, private jwtHelper: JwtHelperService) { }

  canActivate() {
    const token = localStorage.getItem('jwt');

    if (token && !this.jwtHelper.isTokenExpired(token)) {
      const decodedToken = this.jwtHelper.decodeToken(token);
      const roles = decodedToken['http://schemas.microsoft.com/ws/2008/06/identity/claims/role'];

      if (roles === '')
        return false;

      if (roles === 'Manager' || roles === 'Admin')
        return true;

      for (const role of roles) {
        if (role === 'Manager' || role === 'Admin')
          return true;
      }
    }

    this.router.navigate(['home']);
    return false;
  }

  isUserManager() {
    const token: any = localStorage.getItem('jwt');

    if (token && !this.jwtHelper.isTokenExpired(token)) {
      const decodedToken = this.jwtHelper.decodeToken(token);
      const roles = decodedToken['http://schemas.microsoft.com/ws/2008/06/identity/claims/role'];

      if (roles === undefined)
        return false;

      if (roles === 'Manager' || roles === 'Admin')
        return true;

      for (const role of roles) {
        if (role === 'Manager' || role === 'Admin')
          return true;
      }
    }

    return false;
  }
}
