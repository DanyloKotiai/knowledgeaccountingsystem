import { Component, OnInit } from '@angular/core';
import { NgForm } from '@angular/forms';
import { AreaService } from '../services/area.service';
import { ResultService } from '../services/result.service';
import { UserService } from '../services/user.service';

@Component({
  selector: 'app-management',
  templateUrl: './management.component.html',
  styleUrls: ['./management.component.css']
})

export class ManagementComponent implements OnInit {
  FirstName: string = '';
  LastName: string = '';
  ListOfArea: any = [];
  Form: any;
  IsLoaded: boolean = false;
  Report: any;
  IsLoadedReport: boolean = false;
  validMinMax: boolean = true;

  constructor(private userService: UserService, private resultService: ResultService, private areaService: AreaService) {
  }

  ngOnInit(): void {
    const claims = this.userService.getClaimsForUser();
    this.FirstName = claims[0];
    this.LastName = claims[1];

    this.refreshAreaList();
  }

  refreshAreaList() {
    this.areaService.getAreas().subscribe(data => {
      this.ListOfArea = data;
    });
  }

  getResults(form: NgForm) {
    let value = form.value;

    if (value.minResult > value.maxResult) {
      this.validMinMax = false;
      return;
    }
    this.validMinMax = true;

    value.areaNames = [];
    for (let item in value) {
      let tempArr = item.split('');
      let tempName = tempArr[0];
      let tempId = Number(tempArr[1]);

      if (tempName === 'a' && value[item] === true) {
        let area = this.ListOfArea.filter((t: { id: number; }) => t.id === tempId)[0];

        value.areaNames.push(area.name);
      }
    }

    this.Form = value;
    this.IsLoaded = true;
  }

  async generateReport() {
    this.Report = await this.userService.getReportData();
    this.IsLoadedReport = true;
  }
}
