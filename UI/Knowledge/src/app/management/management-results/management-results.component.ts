import { Component, Input, OnChanges, OnInit, SimpleChanges } from '@angular/core';
import { NgForm } from '@angular/forms';
import { ResultService } from 'src/app/services/result.service';

@Component({
  selector: 'app-management-results',
  templateUrl: './management-results.component.html',
  styleUrls: ['./management-results.component.css']
})
export class ManagementResultsComponent implements OnChanges {
  @Input()
  dataForm: any;

  Data: any;

  constructor(private resultService: ResultService) { }

  async ngOnChanges(changes: SimpleChanges) {
    console.log(changes.dataForm.currentValue);
    this.Data = await this.resultService.showResultsForUsersByFiltering(changes.dataForm.currentValue);

    console.log(this.Data);
  }
}
