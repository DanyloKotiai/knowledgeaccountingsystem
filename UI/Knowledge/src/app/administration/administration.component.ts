import { Component, OnInit } from '@angular/core';
import { NgForm } from '@angular/forms';
import { Router } from '@angular/router';
import { AuthGuardAdminService } from '../services/auth-guard-admin.service';
import { UserService } from '../services/user.service';

@Component({
  selector: 'app-administration',
  templateUrl: './administration.component.html',
  styleUrls: ['./administration.component.css']
})

export class AdministrationComponent implements OnInit {
  FirstName: string = '';
  LastName: string = '';
  Email: string = '';
  Users: any = [];
  Roles: any = [];

  Creating: boolean = false;
  validDataForCreating: boolean = true;

  Editing: string = '';
  validDataForEditing: boolean = true;

  Warning: any;

  constructor(private router: Router, private userService: UserService, private authAdmin: AuthGuardAdminService) { }

  ngOnInit(): void {
    const claims = this.userService.getClaimsForUser();
    this.FirstName = claims[0];
    this.LastName = claims[1];
    this.Email = claims[2];

    this.refreshUserList();
  }

  refreshUserList() {
    this.userService.getUsers().subscribe(data => {
      this.Users = data;
      this.Users = this.Users.filter((u: { email: any; }) => u.email !== this.Email);
    });
  }

  async showCreating() {
    this.Roles = await this.userService.getRoles();
    this.Creating = !this.Creating;
  }

  async createUser(form: NgForm) {
    const value = form.value;
    if (value.email === '' || value.password === '') {
      this.validDataForCreating = false;
      return;
    }

    value.roles = [];
    for (let item in value) {
      let tempName = item.substring(0, 2);
      let tempRole = item.slice(2);

      if (tempName === 'r_' && value[item] === true) {
        let role = this.Roles.filter((t: string) => t === tempRole)[0];

        value.roles.push(role);
      }

    }

    await this.userService.createUser(value)
      .then(d => {
        this.validDataForCreating = true;
        window.location.reload();
      })
      .catch(err => {
        console.log(err.error);
        this.validDataForCreating = false;
      });
  }

  async showEditing(id: any) {
    this.Roles = await this.userService.getRoles();

    if (this.Editing === id)
      this.Editing = '';
    else
      this.Editing = id;
  }

  async editUser(form: NgForm, id: any) {
    const value = form.value;
    if (value.email === '' || value.password === '') {
      this.validDataForEditing = false;
      return;
    }

    value.id = id;
    value.roles = [];
    for (let item in value) {
      let tempName = item.substring(0, 2);
      let tempRole = item.slice(2);

      if (tempName === 'r_' && value[item] === true) {
        let role = this.Roles.filter((t: string) => t === tempRole)[0];

        value.roles.push(role);
      }

    }

    await this.userService.editUser(value)
      .then(d => {
        this.validDataForEditing = true;
        window.location.reload();
      })
      .catch(err => {
        console.log(err.error);
        this.validDataForEditing = false;
      });
  }

  showWarning(id: any) {
    this.Warning = id;
  }

  async deleteUser(id: any) {
    await this.userService.deleteUser(id);
    window.location.reload();
  }

  stopDeleting() {
    this.Warning = '';
  }
}

