import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AdministrationComponent } from './administration/administration.component';
import { HomeComponent } from './home/home.component';
import { LoginComponent } from './login/login.component';
import { ManagementComponent } from './management/management.component';
import { ProfileComponent } from './profile/profile.component';
import { AuthGuardAdminService } from './services/auth-guard-admin.service';
import { AuthGuardManagerService } from './services/auth-guard-manager.service';
import { AuthGuardService } from './services/auth-guard.service';
import { SignupComponent } from './signup/signup.component';
import { TestingAreaComponent } from './testing/testing-area/testing-area.component';
import { TestingComponent } from './testing/testing.component';

const routes: Routes = [
  { path: '', component: HomeComponent },
  { path: 'login', component: LoginComponent },
  { path: 'signup', component: SignupComponent },
  { path: 'testing', component: TestingComponent, canActivate: [AuthGuardService] },
  { path: 'testing/:id', component: TestingAreaComponent, canActivate: [AuthGuardService] },
  { path: 'profile', component: ProfileComponent, canActivate: [AuthGuardService] },
  { path: 'management', component: ManagementComponent, canActivate: [AuthGuardManagerService] },
  { path: 'administration', component: AdministrationComponent, canActivate: [AuthGuardAdminService] },
  { path: '**', redirectTo: '/' }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
