import { Component, OnInit } from '@angular/core';
import { NgForm } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { from, Subscription } from 'rxjs';
import { AreaService } from 'src/app/services/area.service';
import { ResultService } from 'src/app/services/result.service';

@Component({
  selector: 'app-testing-area',
  templateUrl: './testing-area.component.html',
  styleUrls: ['./testing-area.component.css']
})
export class TestingAreaComponent implements OnInit {
  list: any = [];
  isValid: boolean = true;
  id: number = 0;
  private subscription: Subscription;

  constructor(private router: Router, private activateRoute: ActivatedRoute, private areaService: AreaService, private resultService: ResultService) {
    this.subscription = activateRoute.params.subscribe(params => this.id = params['id']);
  }

  async ngOnInit(): Promise<void> {
    await this.refreshList(this.id);
  }

  async refreshList(areaId: number) {
    await this.areaService.getSubjectByAreaId(areaId).toPromise()
      .then(async subjects => {
        for (let s of subjects) {
          let obj = {
            sId: s.id,
            sName: s.name,
            tests: []
          }

          await this.areaService.getTestBySubjectId(s.id).toPromise()
            .then(value => {
              obj.tests = value;
              this.list.push(obj);
            });
        }
      })
      .catch(err => {
        this.router.navigate(['testing']);
      })
  }

  async addResult(form: NgForm) {
    let value = form.value;

    this.isValid = true;
    for (let i in value) {
      if (value[i] === "")
        this.isValid = false;
    }

    if (this.isValid) {
      await this.resultService.addResult(form, this.id);
      this.router.navigate(['profile']);
    }
  }
}
