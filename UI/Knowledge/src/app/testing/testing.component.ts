import { Component, OnInit } from '@angular/core';
import { AreaService } from '../services/area.service';

@Component({
  selector: 'app-testing',
  templateUrl: './testing.component.html',
  styleUrls: ['./testing.component.css']
})

export class TestingComponent implements OnInit {

  Areas: any = [];

  constructor(private areaService: AreaService) { }

  ngOnInit(): void {
    this.refreshAreaList();
  }

  refreshAreaList() {
    this.areaService.getAreas().subscribe(data => {
      this.Areas = data;
    })
  }
}
