import { Component } from '@angular/core';
import { Router } from '@angular/router';
import { NgForm } from '@angular/forms';
import { AuthService } from '../services/auth.service';

@Component({
  selector: 'login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})

export class LoginComponent {
  validLogin: boolean = true;

  constructor(private router: Router, private auth: AuthService) { }

  async login(form: NgForm) {

    this.validLogin = await this.auth.login(form);
    if (this.validLogin) {
      this.router.navigate(['/']);
    }
  }
}
