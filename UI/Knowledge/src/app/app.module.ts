import { HttpClientModule } from '@angular/common/http';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { BrowserModule } from '@angular/platform-browser';
import { JwtModule } from '@auth0/angular-jwt';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { HomeComponent } from './home/home.component';
import { LoginComponent } from './login/login.component';
import { AreaService } from './services/area.service';
import { AuthGuardService } from './services/auth-guard.service';
import { AuthService } from './services/auth.service';
import { SignupComponent } from './signup/signup.component';
import { TestingComponent } from './testing/testing.component';
import { TestingAreaComponent } from './testing/testing-area/testing-area.component';
import { ResultService } from './services/result.service';
import { UserService } from './services/user.service';
import { ProfileComponent } from './profile/profile.component';
import { ManagementComponent } from './management/management.component';
import { AuthGuardManagerService } from './services/auth-guard-manager.service';
import { ManagementResultsComponent } from './management/management-results/management-results.component';
import { AuthGuardAdminService } from './services/auth-guard-admin.service';
import { AdministrationComponent } from './administration/administration.component';

export function tokenGetter() {
  return localStorage.getItem('jwt');
}

@NgModule({
  declarations: [
    AppComponent,
    HomeComponent,
    LoginComponent,
    SignupComponent,
    TestingComponent,
    TestingAreaComponent,
    ProfileComponent,
    ManagementComponent,
    ManagementResultsComponent,
    AdministrationComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    HttpClientModule,
    AppRoutingModule,
    JwtModule.forRoot({
      config: {
        tokenGetter: tokenGetter,
        allowedDomains: ['localhost:44351'],
        disallowedRoutes: []
      }
    })
  ],
  providers: [
    AuthGuardService,
    AuthGuardManagerService,
    AuthGuardAdminService,
    AuthService,
    AreaService,
    ResultService,
    UserService
  ],
  bootstrap: [AppComponent]
})

export class AppModule { }
