import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { AreaService } from '../services/area.service';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})

export class HomeComponent implements OnInit {
  Areas: any = [];

  constructor(private router: Router, private areaService: AreaService) { }

  ngOnInit(): void {
    this.refreshAreaList();
  }

  refreshAreaList() {
    this.areaService.getAreas().subscribe(data => {
      this.Areas = data;
    })
  }
}
