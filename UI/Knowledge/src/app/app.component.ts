import { Component } from '@angular/core';
import { AuthGuardAdminService } from './services/auth-guard-admin.service';
import { AuthGuardManagerService } from './services/auth-guard-manager.service';
import { AuthGuardService } from './services/auth-guard.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})

export class AppComponent {
  constructor(private auth: AuthGuardService, private authManager: AuthGuardManagerService, private authAdmin: AuthGuardAdminService) {
  }

  isUserAuthenticated() {
    return this.auth.isUserAuthenticated();
  }

  isUserManager() {
    return this.authManager.isUserManager();
  }

  isUserAdmin() {
    return this.authAdmin.isAdminManager();
  }

  logOut() {
    this.auth.logOut();
  }
}
