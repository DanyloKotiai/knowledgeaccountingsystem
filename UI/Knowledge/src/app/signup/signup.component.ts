import { Component } from '@angular/core';
import { NgForm } from '@angular/forms';
import { Router } from '@angular/router';
import { AuthService } from '../services/auth.service';

@Component({
  selector: 'signup',
  templateUrl: './signup.component.html',
  styleUrls: ['./signup.component.css']
})

export class SignupComponent {
  validData: boolean = true;

  constructor(private router: Router, private auth: AuthService) { }

  async signUp(form: NgForm) {
    this.validData = await this.auth.signUp(form);

    if (this.validData) {
      await this.auth.login(form);
      this.router.navigate(['/']);
    }
  }
}
